# AnimeTracker
Web application build with React.js at front-end, Node.js/Express at Back-end and MongoDB as database.
The purpose is to track anime series, get relevant information about desired series and building series list to track.
data provider right now is "MyAnimeList"

# dependencies:
## Front-end: 
1. "@emotion/react": "^11.10.5"
2. "@emotion/styled": "^11.10.5"
3. "@mui/material": "^5.10.15"
4. "axios": "^1.1.3"
5. "react": "^18.2.0"
6. "react-dom": "^18.2.0"
7. "react-router-dom": "^6.6.1"
8. "react-scripts": "^5.0.1"
9. "web-vitals": "^2.1.4"
10. "react-cookie": "^4.1.1"
11. "@mui/icons-material": "^5.11.0"

## Back-end: 
1. "node": "^v18.12.1"
2. "bcrypt": "^5.1.0"
3. "cors": "^2.8.5"
4. "express": "^4.18.2"
5. "express-validator": "^6.14.2"
6. "mongoose": "^6.8.1"
8. "nodemon": "^2.0.20"
9. "jsonwebtoken": "^9.0.0"
10. "cookie-parser": "^1.4.6"
11. "axios": "^1.2.3"
