import Button from "@mui/material/Button";
import { useState } from "react";
import apiService from "../../Services/API/api";
import useLogout from "../../Services/Auth/Hooks/useLogout";
import AlertDialog from "../Shared/alertDialog";
import style from "./deleteButton.module.css";

const DeleteButton = () => {
  const [isDeleting, setIsDeleting] = useState(false);
  const [buttonClass, setButtonClass] = useState(style.delete_button);
  const [showDialog, setShowDialog] = useState(false);
  const [showDialogProgress, setShowDialogProgress] = useState(false);
  const [dialogTitle, setDialogTitle] = useState("Delete Account");
  const [textMessage, setTextMessage] = useState("");
  const [deleteQuestion, setDeleteQuestion] = useState(false);
  const handleLogout = useLogout();

  const deleteAccount = async () => {
    try {
      const response = await apiService.UsersService.deleteUser();
      if (response.status === 200) {
        return true;
      }
      return false;
    } catch (error) {
      return false;
    }
  };

  const handleClick = () => {
    setShowDialogProgress(false);
    setTextMessage("Are you sure you want to delete your account?");
    setDeleteQuestion(true);
    setShowDialog(true);
  };

  const closeAlertHandlerDelete = async () => {
    setTimeout(() => {
      setShowDialog(false);
    }, 20);
    try {
      const deleted = await deleteAccount();
      setButtonClass((current) => {
        return `${current} ${style.deleting}`;
      });
      setIsDeleting(true);
      await new Promise((resolve) => setTimeout(resolve, 500)); // Wait 500ms to render animation
      if (deleted) {
        await handleLogout();
      }
    } catch (error) {
      setDialogTitle("Deleting Failed");
      setTextMessage(error.response.data.message);
      setDeleteQuestion(false);
      setShowDialog(true);
    } finally {
      setButtonClass(style.delete_button);
      setIsDeleting(false);
    }
  };

  const closeAlertHandler = () => {
    setIsDeleting(false);
    setTimeout(() => {
      setShowDialog(false);
    }, 20);
  };

  return (
    <Button
      onClick={handleClick}
      className={buttonClass || style.delete_button}
      disabled={isDeleting}
      sx={{
        bgcolor: "myList.main",
        color: "secondary.text",
        padding: 0,
      }}
    >
      <span className={style.button_text}>
        {isDeleting ? "Deleting ..." : "Delete User"}
      </span>
      <span className={style.animation}>
        <span className={style.paper_wrapper}>
          <span className={style.paper}></span>
        </span>
        <span className={style.shredded_wrapper}>
          <span className={style.shredded}></span>
        </span>
        <span className={style.lid}></span>
        <span className={style.can}>
          <span className={style.filler}></span>
        </span>
      </span>
      <AlertDialog
        showDialog={showDialog}
        showDialogProgress={showDialogProgress}
        title={dialogTitle}
        closeHandler={closeAlertHandlerDelete}
        closeHandlerRedirect={closeAlertHandler}
        redirectMessage="No"
        showRedirectButton={deleteQuestion}
        description={textMessage}
      />
    </Button>
  );
};

export default DeleteButton;
