import { useEffect, useState } from "react";
import apiService from "../../Services/API/api";
import AnimeCard from "../AnimeCard/animeCard";
import style from "./myList.module.css";

const MyList = () => {
  const [userList, setUserList] = useState({});
  const [listCount, setListCount] = useState(-1);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    setIsError(false);
    apiService.SeriesService.getMyList()
      .then((response) => {
        setUserList(response.list);
        setListCount(response.count);
        setIsError(false);
      })
      .catch((err) => {
        setIsError(true);
        setListCount(-1);
      });
  }, []);

  return (
    <div className={style.my_list}>
      <h1 className={style.headline}>My List</h1>
      {isError ? (
        <svg className={style.errorContainer}>
          <text>
            <tspan x="42" y="160">
              Something went wrong
            </tspan>
          </text>
        </svg>
      ) : listCount === 0 ? (
        <div className={style.emptyContainer}>
          Your list is empty (⌒_⌒)
          <br />
          Try adding anime and come back
        </div>
      ) : (
        <div className={style.container}>
          {Object.values(userList).map((anime) => (
            <AnimeCard key={anime._id} details={anime.animeId} />
          ))}
        </div>
      )}
    </div>
  );
};

export default MyList;
