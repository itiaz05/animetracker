import GitHubIcon from "@mui/icons-material/GitHub";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import Divider from "@mui/material/Divider";
import { Link } from "react-router-dom";
import gitlabLogo from "../../../Resources/Web/icons8-gitlab-color-32.png";
import style from "./footer.module.css";

const Footer = () => {
  const version = process.env.REACT_APP_VERSION;
  return (
    <>
      <Divider sx={{ zIndex: "5", position: "relative", mt: "10px" }} />
      <div className={style.footer_container}>
        <div>My Anime Tracker &copy;</div>

        <div>Version: {version}</div>

        <Link className={style.about} to={"/about"}>
          About us
        </Link>

        <div className={style.follow_us}>
          Follow us on:
          <a
            href="https://gitlab.com/itiaz05"
            target="_blank"
            rel="noreferrer"
          >
            <img src={gitlabLogo} alt="gitlab" className={style.media_logo} />
          </a>
          <a href="https://github.com/itiaz05" target="_blank" rel="noreferrer">
            <GitHubIcon
              className={style.media_logo}
              sx={{ color: "secondary.text" }}
            />
          </a>
          <a
            href="https://www.linkedin.com/in/itai-simhony/"
            target="_blank"
            rel="noreferrer"
          >
            <LinkedInIcon sx={{ color: "blue" }} className={style.media_logo} />
          </a>
        </div>
      </div>
    </>
  );
};

export default Footer;
