import { useEffect, useReducer } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import ListItem from "../AnimeList/listItem";
import style from "./listPage.module.css";

// Define the reducer function
const reducer = (state, action) => {
  switch (action.type) {
    case "SET_LIST":
      return {
        ...state,
        genre: action.payload.genre,
        length: action.payload.length,
        list: action.payload.list,
      };
    case "SET_ERROR":
      return {
        ...state,
        isError: true,
      };
    default:
      return state;
  }
};

const ListPage = () => {
  const { state } = useLocation();
  const navigate = useNavigate();
  const [stateData, dispatch] = useReducer(reducer, {
    genre: state?.genre || null,
    length: state?.length || -1,
    list: state?.list || {},
    isError: false,
  });

  useEffect(() => {
    if (state === undefined || state === null) {
      navigate("/not-found");
    }
    try {
      dispatch({
        type: "SET_LIST",
        payload: {
          genre: state.genre,
          length: state.length,
          list: state.list,
        },
      });
    } catch (error) {
      dispatch({ type: "SET_ERROR" });
    }
  }, []);

  const { genre, length, list, isError } = stateData;

  return (
    <div className={style.my_list}>
      <h1 className={style.headline}>'{genre}' Page</h1>
      {isError ? (
        <svg className={style.errorContainer}>
          <text>
            <tspan x="42" y="160">
              Something went wrong
            </tspan>
          </text>
        </svg>
      ) : length === 0 ? (
        <div className={style.emptyContainer}>Your list is empty (⌒_⌒)</div>
      ) : (
        <div className={style.container}>
          {list.map((anime) => (
            <ListItem key={anime?._id} animeDetails={anime} />
          ))}
        </div>
      )}
    </div>
  );
};

export default ListPage;
