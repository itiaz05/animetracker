import Button from "@mui/material/Button";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import apiService from "../../../Services/API/api";
import style from "./animeList.module.css";
import ListItem from "./listItem";

const AnimeList = ({ genre, list, cardSizes }) => {
  const navigate = useNavigate();
  const [cardsHeight, setCardsHeight] = useState("220px");
  const [cardsWidth, setCardsWidth] = useState("150px");
  const navToGenrePage = async () => {
    const response = await apiService.SeriesService.getListByGenre(genre);
    navigate(`/listPage/${genre}`, { state: response });
  };

  useEffect(() => {
    if (cardSizes?.cardsHeight) {
      setCardsHeight(cardSizes.cardsHeight);
    }
    if (cardSizes?.cardsWidth) {
      setCardsWidth(cardSizes.cardsWidth);
    }
  }, [cardSizes]);

  return (
    <div className={style.anime_list}>
      <div className={style.more_button}>
        <div className={style.more_button_title}>{genre ? genre : null}</div>
        <Button onClick={navToGenrePage} variant="text">
          More Like This
        </Button>
      </div>
      <div className={style.list_container}>
        {list.map((anime) => (
          <ListItem
            key={anime?._id}
            animeDetails={anime}
            cardsHeight={cardsHeight}
            cardsWidth={cardsWidth}
          />
        ))}
      </div>
    </div>
  );
};

export default AnimeList;
