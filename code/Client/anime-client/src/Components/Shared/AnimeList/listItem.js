import Card from "@mui/material/Card";
import { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import style from "./listItem.module.css";

const ListItem = ({
  animeDetails,
  cardsHeight = "220px",
  cardWidth = "150px",
}) => {
  const [isError, setIsError] = useState(false);
  const navigate = useNavigate();

  const navToPage = useCallback(() => {
    if (!isError) {
      navigate(`/animePage/${animeDetails?.title}`, { state: animeDetails });
    }
  }, [navigate, animeDetails, isError]);

  useEffect(() => {
    setIsError(!animeDetails);
  }, [animeDetails]);

  return (
    <div className={style.card_container} onClick={navToPage}>
      <Card
        sx={{
          height: cardsHeight,
          width: cardWidth,
          backgroundImage: `url(${animeDetails?.image})`,
          backgroundSize: "cover",
          position: "relative",
        }}
        className={style.card_image}
      >
        <div className={style.card_rating}>
          {animeDetails?.rating?.toFixed(2)}
        </div>
      </Card>
      <div className={style.card_title}>{animeDetails?.title}</div>
    </div>
  );
};

export default ListItem;
