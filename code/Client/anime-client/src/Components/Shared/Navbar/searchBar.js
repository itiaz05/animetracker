import SearchIcon from "@mui/icons-material/Search";
import Button from "@mui/material/Button";
import Input from "@mui/material/Input";
import { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import apiService from "../../../Services/API/api";
import AlertDialog from "../alertDialog";
import style from "./searchBar.module.css";

const SearchBar = () => {
  const searchTerm = useRef();
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  const handleSearch = (e) => {
    setIsLoading(true);
    setIsError(false);
    e.preventDefault();
    if (searchTerm.current.value === "") {
      setIsLoading(false);
      return;
    }
    searchTerm.current.value = searchTerm.current.value.replace("/", "_");
    apiService.SeriesService.searchFor(searchTerm.current.value)
      .then((response) => {
        let url = response.title.replace("/", " ");
        navigate(`/animePage/${url}`, { state: response });
      })
      .catch((err) => {
        setIsError(true);
        console.log(err);
      })
      .finally(() => {
        searchTerm.current.value = "";
        setIsLoading(false);
      });
  };

  return (
    <form className={style.search_bar} onSubmit={handleSearch}>
      <Input
        type="search"
        placeholder="Search specific anime"
        className={style.search_input}
        inputRef={searchTerm}
        disableUnderline
        sx={{ color: "searchText.main" }}
      />
      <SearchIcon className={style.search_icon} />
      <Button
        className={style.search_button}
        variant="contained"
        type="submit"
        {...(isLoading ? { disabled: true } : { disabled: false })}
        {...(isError ? { sx: { bgcolor: "error.main" } } : {})}
      >
        {isLoading ? (
          <div className={style.custom_loader}></div>
        ) : (
          <span>Search</span>
        )}
      </Button>
      <AlertDialog
        showDialog={isError}
        title={"Search Failed"}
        showProgress={isLoading}
        description={"No anime found with that name"}
        setOpen={setIsError}
        closeHandler={() => setIsError(false)}
      />
    </form>
  );
};

export default SearchBar;
