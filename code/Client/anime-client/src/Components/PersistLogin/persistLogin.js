import { useEffect, useState } from "react";
import { Outlet } from "react-router-dom";
import { useAuth } from "../../Services/Auth/authProvider";
import useRefreshToken from "../../Services/Auth/Hooks/useRefreshToken";

const PersistLogin = () => {
  const [isLoading, setIsLoading] = useState(true);
  const refreshToken = useRefreshToken();
  const { auth, setAuth } = useAuth();

  useEffect(() => {
    console.log("in use effect of persist login");

    const verifyRefreshToken = async () => {
      try {
        await refreshToken();
      } catch (error) {
        console.log(error);
      } finally {
        setIsLoading(false);
      }
    };
    const username = JSON.parse(localStorage.getItem("MAT-U-N"));
    if (username) {
      setAuth((prevState) => ({ ...prevState, user: username.toString() }));
    }
    !auth?.token ? verifyRefreshToken() : setIsLoading(false);
  }, []);

  return <>{isLoading ? <div>Loading...</div> : <Outlet />}</>;
};

export default PersistLogin;
