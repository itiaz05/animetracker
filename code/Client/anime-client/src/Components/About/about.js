import style from "./about.module.css";

const About = () => {
  return (
    <div className={style.aboutContainer}>
      <h1 className={style.aboutHeader}>About Us</h1>

      <p>
        'My Anime Tracker' is an open source web application that allows users
        to track their favorite anime series.
      </p>
      <p>
        The application is designed to be a simple and easy to use tool for
        anime fans.
      </p>

      <p>
        The application is currently in beta version and is still under
        development
      </p>

      <p>
        And can be found on&nbsp;
        <a
          href="https://gitlab.com/itiaz05/animetracker"
          target="_blank"
          rel="noreferrer"
        >
          Gitlab
        </a>
        .
      </p>

      <p>
        If you have any questions or suggestions, please contact us
        through&nbsp;
        <a href="mailto:itiaz05@gmail.com" target="_blank">
          Email
        </a>
      </p>

      <p>
        <a
          href="https://www.linkedin.com/in/itai-simhony/"
          target="_blank"
          rel="noreferrer"
        >
          Itai Simhony
        </a>
      </p>
    </div>
  );
};

export default About;
