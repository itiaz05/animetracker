import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import style from "./notFound.module.css";

const NotFound = () => {
  const navigate = useNavigate();
  const goBack = () => {
    navigate(-1);
  };
  return (
    <div className={style.notFound}>
      <div className={style.notFoundMessage}>
        <h1>Page Not Found - 404 </h1>
        <h2>Oops, looks like you have followed Zoro</h2>
        <h2>To an unknown path</h2>
        <Button size="large" onClick={() => navigate("/")}>
          Go to Homepage
        </Button>
        <Button onClick={goBack}>Or Go Back</Button>
      </div>
    </div>
  );
};

export default NotFound;
