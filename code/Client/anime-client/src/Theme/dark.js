import { createTheme } from "@mui/material/styles";

const darkTheme = createTheme({
  palette: {
    mode: "dark",
    primary: {
      main: "#00e5ff",
    },
    secondary: {
      main: "#f50057",
      text: "#ffffff",
      bg: "#000000",
    },
    success: {
      main: "#4cb150",
    },
    info: {
      main: "#ffffff",
      light: "#283593",
    },
    text: {
      primary: "#4dd0e1",
    },
    myList: {
      main: "#FF0000",
    },
    notMyList: {
      main: "#848383",
    },
    searchText: {
      main: "#000000",
      dark: "#000000",
      light: "#000000",
    },
    warning: {
      main: "#d50000",
    },
  },
});

export default darkTheme;
