import { createTheme } from "@mui/material/styles";

const lightTheme = createTheme({
  palette: {
    mode: "light",
    primary: {
      main: "#3f51b5",
    },
    secondary: {
      main: "#f50057",
      text: "#000000",
      bg: "#ffffff",
    },
    info: {
      main: "#2196f3",
      light: "#282c34",
    },
    myList: {
      main: "#FF0000",
    },
    notMyList: {
      main: "#848383",
    },
    searchText: {
      main: "#000000",
    },
    warning: {
      main: "#d50000",
    },
  },
});

export default lightTheme;
