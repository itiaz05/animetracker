import { useEffect } from "react";
import serverAxios from "../../API/axiosInstance";
import { useAuth } from "../authProvider";
import useRefreshToken from "./useRefreshToken";

const useAxiosAuth = () => {
  const refreshToken = useRefreshToken();
  const { auth } = useAuth();

  useEffect(() => {
    let isRefreshing = false;
    console.log("in use effect of axios auth");

    const requestInterceptor = serverAxios.interceptors.request.use(
      (config) => {
        if (!config.headers["Authorization"]) {
          config.headers["Authorization"] = `Bearer ${auth?.token}`;
        }
        return config;
      },
      (error) => Promise.reject(error)
    );

    const responseInterceptor = serverAxios.interceptors.response.use(
      (response) => {
        if (response?.data?.error === "Refresh token expired") {
          return Promise.reject("Refresh token expired");
        }
        return response;
      },
      async (error) => {
        const originalRequest = error?.config;
        try {
          if (error?.response?.status === 403 && !originalRequest?.sent && !isRefreshing) {
            isRefreshing = true;
            originalRequest.sent = true;
            if (originalRequest?.retry) originalRequest.retry = false;
            const newAccessToken = await refreshToken();
            if (newAccessToken) {
              originalRequest.headers = {
                ...originalRequest.headers,
                Authorization: `Bearer ${newAccessToken}`
              };
              return serverAxios(originalRequest);
            }
          }
        } catch (error) {
          console.log(error);
        }
        return Promise.reject(error);
      }
    );

    return () => {
      serverAxios.interceptors.request.eject(requestInterceptor);
      serverAxios.interceptors.response.eject(responseInterceptor);
    };
  }, [auth, refreshToken]);

  return serverAxios;
};

export default useAxiosAuth;
