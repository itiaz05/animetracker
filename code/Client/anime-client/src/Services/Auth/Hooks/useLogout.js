import apiService from "../../API/api";
import { useAuth } from "../authProvider";

const useLogout = () => {
  const { setAuth } = useAuth();

  const handleLogout = async () => {
    try {
      const response = await apiService.UsersService.logout();
      if (response?.status !== 200) {
        throw { message: response.message, status: response?.status };
      }
    } catch (error) {
      console.log(error);
    } finally {
      setAuth({});
      localStorage.removeItem("MAT-U-N");
    }
  };
  return handleLogout;
};

export default useLogout;
