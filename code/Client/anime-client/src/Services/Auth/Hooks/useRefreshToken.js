import ApiService from "../../API/api";
import { useAuth } from "../authProvider";
import useLogout from "./useLogout";

const useRefreshToken = () => {
  const { setAuth } = useAuth();
  const handleLogout = useLogout();

  const refreshToken = async () => {
    try {
      const response = await ApiService.UsersService.refreshToken();
      setAuth((prevState) => ({ ...prevState, token: response.token }));
      return response.token;
    } catch (error) {
      if (error?.response?.data?.error === "Refresh token expired") {
        handleLogout();
      }
    }
  };
  return refreshToken;
};

export default useRefreshToken;
