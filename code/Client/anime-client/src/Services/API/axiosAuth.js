import useAxiosAuth from "../Auth/Hooks/useAxiosAuth";

const AxiosAuth = () => {
  useAxiosAuth();
};

export default AxiosAuth;
