import { useCallback } from "react";

const useGenres = () => {
  const destructureGenres = useCallback((genresArr) => {
    let genres = [];
    if (!genresArr) return genres;

    return genresArr.map((genre) => genre["name"]);
  }, []);

  return { destructureGenres };
};

export default useGenres;
