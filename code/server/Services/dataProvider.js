import axios from "axios";
import { Errors } from "../Shared/index.js";
import { filesHelpers, stringsHelpers } from "../Utils/index.js";

const { InternalServerError } = Errors;

// Definitions
const options = {
  method: "GET",
  baseURL: "https://api.myanimelist.net/v2/",
  headers: {
    "X-MAL-CLIENT-ID": process.env.MAL_CLIENT_ID
  }
};

// Data provider API class
export default class DataProvider {
  // Get series's id
  static async getCode(searchTerm) {
    if (!searchTerm) {
      throw new InternalServerError({ message: "Search term invalid" });
    }

    try {
      const MAX_RESULTS = 10;
      const cleanedSearchTerm = stringsHelpers.cleanInput(searchTerm, { capitalize: true });
      options["url"] ="anime?" ;
      const fields = "num_episodes,media_type,alternative_titles";
      options["params"] = {
        q: cleanedSearchTerm,
        limit: MAX_RESULTS,
        fields: fields
      };

      let response = await axios.request(options);
      let seriesId = await DataProvider.getSeriesId(response.data.data, searchTerm, MAX_RESULTS);

      if (seriesId === "no results") {
        options["params"] = { ...options["params"], offset: MAX_RESULTS };
        response = await axios.request(options);
        seriesId = await DataProvider.getSeriesId(response.data.data, searchTerm, MAX_RESULTS);
      }

      if (seriesId === "no results") {
        throw new Error(seriesId);
      }

      return seriesId;
    } finally {
      delete options["params"];
      delete options["url"];
    }
  }

  // Get search's details
  static async getDetails(searchTerm) {
    if (!searchTerm) {
      throw new Error("search term invalid");
    }

    try {
      const animeId = await this.getCode(searchTerm);

      const url = `anime/${animeId}`;
      options["url"] = url;
      options["params"] = {
        fields: "title,main_picture,start_date,num_episodes,synopsis,mean,alternative_titles,genres,status"
      };

      const response = await axios.request(options);

      const details = {
        title: response.data["title"],
        alternativeTitle: response.data["alternative_titles"]["en"],
        year: response.data["start_date"].substring(0, 4),
        numberOfEpisodes: response.data["num_episodes"],
        rating: response.data["mean"],
        image: response.data["main_picture"]["medium"],
        description: response.data["synopsis"],
        genres: response.data["genres"],
        status: response.data["status"]
      };

      filesHelpers.saveJSON(`${response.data["title"]} details`, response.data);

      return details;
    } finally {
      delete options["params"];
      delete options["url"];
    }
  }

  // Filter results from provider to find wanted series
  static async getSeriesId(response, searchQuery, MAX_RESULTS) {
    if (!response || !searchQuery) {
      throw new Error("Invalid input");
    }

    for (let i = 0; i < MAX_RESULTS; i++) {
      const cleanedAltTitle = stringsHelpers.cleanInput(response[i].node.alternative_titles.en);

      const namesMatch =
        response[i].node.title.toLowerCase() === searchQuery.toLowerCase() || cleanedAltTitle.toLowerCase() === searchQuery.toLowerCase();
      const verySimilar =
        cleanedAltTitle.toLowerCase().includes(searchQuery.toLowerCase()) &&
        cleanedAltTitle.length in [searchQuery.length - 1, searchQuery.length, searchQuery.length + 1];

      const isSeries = (response[i].node.media_type === "tv" || response[i].node.media_type === "ona") && response[i].node.num_episodes > 1;
      if (isSeries && (namesMatch || verySimilar)) {
        return response[i].node.id;
      }
    }

    return "no results";
  }
}
