import axios from "axios";

const apiRequest = async (options) => {
  try {
    const response = await axios.request(options);
    return response.data;
  } catch (error) {
    console.log(error);
    return null;
  }
};

export { apiRequest };
