import { createClient } from "redis";
import { Errors } from "../Shared/index.js";

const { InternalServerError } = Errors;

let redisClient;
let isConnecting = false;

const connectRedis = async () => {
  if (!redisClient && !isConnecting) {
    isConnecting = true;
    try {
      redisClient = createClient({
        host: process.env.REDIS_DOCKER_ENDPOINT,
        port: process.env.REDIS_DOCKER_PORT
      });
      redisClient.on("error", (err) => console.log("Redis Client Error: ", err));
      await redisClient.connect();
    } catch (error) {
      console.error("Failed to connect to Redis:", error);
      redisClient = null;
    } finally {
      isConnecting = false;
    }
  }
  return redisClient;
};

const disconnectRedis = async () => {
  if (!redisClient) {
    console.log("Can not disconnect, Redis client is not initialized");
    return;
  }

  try {
    await redisClient.quit();
    console.log("Redis client disconnected gracefully.");
  } catch (error) {
    console.error("Error disconnecting Redis client:", error);
  } finally {
    redisClient = null;
  }
};

const getValue = async (key) => {
  const client = await connectRedis();
  if (!client) {
    throw new InternalServerError({ message: "Unable to connect to Redis", context: { redisKey: key } });
  }

  return await client.get(key);
};

const storeRecord = async (key, value, options) => {
  const client = await connectRedis();
  if (!client) {
    throw new InternalServerError({ message: "Unable to connect to Redis", context: { redisKey: key } });
  }

  const { expiresInMs } = options;
  await client.set(key, value, { EX: expiresInMs });
};

const deleteRecord = async (key) => {
  const client = await connectRedis();
  if (!client) {
    throw new InternalServerError({ message: "Unable to connect to Redis", context: { redisKey: key } });
  }

  await client.del(key);
};

const getTTL = async (key) => {
  const client = await connectRedis();
  if (!client) {
    throw new InternalServerError({ message: "Unable to connect to Redis", context: { redisKey: key } });
  }

  const ttl = await client.TTL(key); // in seconds
  if (ttl === -1 || ttl === -2) {
    return null;
  }
  return ttl;
};

export default { disconnectRedis, getValue, storeRecord, deleteRecord, getTTL };
