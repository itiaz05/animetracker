import { Errors } from "../Shared/index.js";
import { filesHelpers, stringsHelpers } from "../Utils/index.js";
import { apiRequest } from "./index.js";

const { InternalServerError } = Errors;

export default class InformationFetcher {
  static #MAX_RESULTS = 10;
  static #ANIME_ID_SEARCH_FIELDS = "num_episodes,media_type,alternative_titles";
  static #ANIME_DETAILS_SEARCH_FIELDS = "title,main_picture,start_date,num_episodes,synopsis,mean,alternative_titles,genres,status";

  static #searchAtProvider = async (url, params = {}) => {
    try {
      return await apiRequest({
        method: "GET",
        baseURL: "https://api.myanimelist.net/v2/",
        headers: {
          "X-MAL-CLIENT-ID": process.env.MAL_CLIENT_ID,
          "Content-Type": "application/json",
          Accept: "*/*"
        },
        url,
        params
      });
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  static #extractAnimeDetails = (data) => ({
    title: data?.title,
    alternativeTitle: data?.alternative_titles?.en,
    year: data?.start_date.substring(0, 4),
    numberOfEpisodes: data?.num_episodes,
    rating: data?.mean,
    image: data?.main_picture.medium,
    description: data?.synopsis,
    genres: data?.genres,
    status: data?.status
  });

  static getAnimeId = async (searchTerm) => {
    if (!searchTerm) {
      throw new InternalServerError({ message: "Invalid search term" });
    }

    const cleanedSearchTerm = stringsHelpers.cleanInput(searchTerm, { capitalize: true });

    for (let offset = 0; offset < 3 * this.#MAX_RESULTS; offset += this.#MAX_RESULTS) {
      const animeIdsList = await this.#searchAtProvider("anime", {
        q: cleanedSearchTerm,
        limit: this.#MAX_RESULTS,
        fields: this.#ANIME_ID_SEARCH_FIELDS,
        offset
      });

      const seriesId = this.#findMatchingAnimeId(animeIdsList?.data, cleanedSearchTerm);
      if (seriesId) {
        return seriesId;
      }
    }

    return null;
  };

  static #findMatchingAnimeId = (animeList, searchTerm) => {
    for (const animeDetails of animeList) {
      try {
        const { node: anime } = animeDetails;
        const isMatch = this.#isAnimeMatching(anime, searchTerm);

        if (isMatch) {
          return anime?.id;
        }
      } catch (error) {
        if (error.message === "Input is invalid") {
          continue;
        }
        throw error;
      }
    }

    return null;
  };

  static #isAnimeMatching = (anime, searchTerm) => {
    if (!anime) return;
    const cleanedAltTitle = stringsHelpers.cleanInput(anime.alternative_titles?.en);

    const loweredSearchTerm = searchTerm.toLowerCase();
    const loweredTitle = anime.title.toLowerCase();
    const loweredAltTitle = cleanedAltTitle.toLowerCase();

    const namesMatch = loweredTitle === loweredSearchTerm || loweredAltTitle === loweredSearchTerm;

    const verySimilar =
      loweredAltTitle.includes(loweredSearchTerm) &&
      [loweredSearchTerm.length - 1, loweredSearchTerm.length, loweredSearchTerm.length + 1].includes(loweredAltTitle.length);

    const isSeries = (anime.media_type === "tv" || anime.media_type === "ona") && anime.num_episodes > 1;

    return isSeries && (namesMatch || verySimilar);
  };

  static getAnimeDetails = async (searchTerm) => {
    if (!searchTerm) {
      throw new InternalServerError({ message: "Invalid search term" });
    }

    const animeId = await this.getAnimeId(searchTerm);

    if (!animeId) {
      return null;
    }

    const animeData = await this.#searchAtProvider(`anime/${animeId}`, { fields: this.#ANIME_DETAILS_SEARCH_FIELDS });

    const animeDetails = this.#extractAnimeDetails(animeData);
    return animeDetails;
  };
}
