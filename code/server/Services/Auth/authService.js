import { Errors } from "../../Shared/index.js";
import redisHandler from "../redisHandler.js";
import jwtHandler from "./jwtHandler.js";

const { ValidationError } = Errors;

/**
 * @param {Object} params - Parameters for storing the refresh token.
 * @param {string} params.userId - The ID of the user.
 * @param {string} params.token - The refresh token to store.
 * @param {number} [params.expiresInMs=604800000] - The expiration time of the token in milliseconds (defaults to 7 days).
 */
const storeRefreshToken = async ({ userId, token, expiresInMs = 60 * 60 * 24 * 7 }) => {
  const key = `refreshToken:${userId}`;
  await redisHandler.storeRecord(key, token, { expiresInMs });
};

/**
 * @param {string} userId - The ID of the user.
 * @returns {Promise<string|null>} - Resolves with the refresh token, or null if not found.
 */
const getRefreshToken = async (userId) => {
  const key = `refreshToken:${userId}`;
  return await redisHandler.getValue(key);
};

const deleteRefreshToken = async (userId) => {
  const key = `refreshToken:${userId}`;
  await redisHandler.deleteRecord(key);
};

/**
 * @param {Object} tokenPayload - The payload to be used for generating the access token.
 * @param {Object} [refreshTokenPayload] - The payload to be used for generating the refresh token.
 * @returns {Promise<string>} - Resolves with the access token.
 */
const executeLoginActions = async (tokenPayload, refreshTokenPayload) => {
  const oneWeekInSec = 60 * 60 * 24 * 7;
  const token = jwtHandler.generateToken(tokenPayload);
  const refreshToken = jwtHandler.generateToken(refreshTokenPayload || tokenPayload, { expiresIn: oneWeekInSec, isRefresh: true });
  await storeRefreshToken({ userId: tokenPayload.id.toString(), token: refreshToken.toString() });
  return { token, refreshToken };
};

/**
 * @param {Object} user - The user to refresh the token for.
 * @param {String} existingRefreshToken - Attached refresh token.
 * @returns {Promise<String>} New access token.
 */
const refreshToken = async (user, existingRefreshToken) => {
  const dbRefreshToken = await getRefreshToken(user.id);
  const isTokenValid = existingRefreshToken === dbRefreshToken;
  if (!isTokenValid) {
    throw new ValidationError({ message: "Invalid refresh token", context: { userId: user.id } });
  }

  const tokenPayload = {
    id: user.id,
    username: user.username,
    email: user.email
  };
  return await executeLoginActions(tokenPayload);
};

export default { storeRefreshToken, getRefreshToken, deleteRefreshToken, executeLoginActions, refreshToken };
