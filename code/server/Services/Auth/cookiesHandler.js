const DEFAULT_OPTIONS = {
  httpOnly: true,
  sameSite: "strict",
  secure: true
};

/**
 * Sets a cookie on the response object.
 * @param {Object} res - The response object.
 * @param {string} name - The name of the cookie.
 * @param {string} value - The value of the cookie.
 * @param {Object} options - Options for the cookie (e.g., httpOnly, maxAge).
 */
const setCookie = (res, name, value, options = {}) => {
  res.cookie(name, value, {
    ...DEFAULT_OPTIONS,
    ...options
  });
};

/**
 * Clears a cookie from the response object.
 * @param {Object} res - The response object.
 * @param {string} name - The name of the cookie to clear.
 * @param {Object} options - Options for clearing the cookie (e.g., httpOnly, maxAge).
 */
const clearCookie = (res, name, options = {}) => {
  res.clearCookie(name, {
    ...DEFAULT_OPTIONS,
    ...options
  });
};

export default { setCookie, clearCookie };
