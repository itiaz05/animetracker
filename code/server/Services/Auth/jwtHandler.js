import jwt from "jsonwebtoken";
import util from "util";

const JWT_SECRET = process.env.JWT_SECRET;
const REFRESH_SECRET = process.env.JWT_REFRESH_SECRET;

/**
 * @param {String} token The token to decode.
 * @param {boolean} isRefresh - Indicates if the desired token is refresh or normal token.Defaults to false.
 * @returns {Promise<boolean>} A Promise that resolves to the decoded payload of the token if the token is valid, error otherwise.
 */
const decodeTokenAsync = async (token, isRefresh = false) => {
  const asyncJwtVerify = util.promisify(jwt.verify);
  return await asyncJwtVerify(token, isRefresh ? REFRESH_SECRET : JWT_SECRET, { ignoreExpired: true });
};

/**
 *
 * @param {String} token The token to decode.
 * @param {boolean} isRefresh - Indicates if the desired token is refresh or normal token.Defaults to false.
 * @returns {Object} Decoded payload of the token if the token is valid, error otherwise.
 */
const decodeToken = (token, isRefresh = false) => jwt.verify(token, isRefresh ? REFRESH_SECRET : JWT_SECRET);

/**
 * @param {Object} payload - The payload to be encoded into the token.
 * @param {Object} options
 * @param {String} options.expiresIn - The expiration time of the token (e.g '30m'/'10d').Defaults to "30m".
 * @param {boolean} options.isRefresh - Indicates if the desired token is refresh or normal token.Defaults to false.
 * @returns {string} The generated token.
 */
const generateToken = (payload, { expiresIn = "15m", isRefresh = false } = {}) =>
  jwt.sign(payload, isRefresh ? REFRESH_SECRET : JWT_SECRET, { expiresIn });

export default { decodeTokenAsync, decodeToken, generateToken };
