import cookieParser from "cookie-parser";
import cors from "cors";
import express from "express";
import http from "http";
import mongoose from "mongoose";
import animeRoutes from "./Components/Anime/routes.js";
import animeStateRoutes from "./Components/AnimeState/routes.js";
import usersRoutes from "./Components/Users/routes.js";
import { errorHandler, redisShutdown } from "./Middlewares/index.js";

// Definitions
const corsOptions = {
  origin: process.env.ALLOWED_ORIGINS, //access-control-allow-origin
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200,
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE" //access-control-allow-methods
};

const PORT = process.env.PORT || 8000;

// Server app start
const app = express();

// Database connection
const dbUri = `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@animetrackerdb.dsesd8p.mongodb.net/?retryWrites=true&w=majority`;
async function connectToDB() {
  try {
    await mongoose.connect(dbUri);
    console.log("Connected to DB");
  } catch (err) {
    console.log("Error connecting to DB: " + err);
  }
}
connectToDB();

// Middleware
app.use(cors(corsOptions)); // Cors
app.use(express.json()); // JSON parser
app.use(cookieParser()); // Cookie parser

// Routes
app.use("/api/animeState", animeStateRoutes);
app.use("/api/anime", animeRoutes);
app.use("/users/", usersRoutes);

// Errors Handler Middleware
app.use(errorHandler);

// App listen (create server)
const server = http.createServer(app);
server.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});

// Redis graceful shutdown middleware
redisShutdown(server);
