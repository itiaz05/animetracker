class CustomError extends Error {
  constructor({ message = "UNHANDLED_ERROR", statusCode = 500, context = {} }) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
    this.statusCode = statusCode;
    this.context = context;
  }
}

class InternalServerError extends CustomError {
  constructor({ message = "Internal Server CustomError", context = {} }) {
    super({ message, statusCode: 500, context });
  }
}

class NotFound extends CustomError {
  constructor({ message = "Not Found", context = {} }) {
    super({ message, statusCode: 404, context });
  }
}

class BadRequest extends CustomError {
  constructor({ message = "Bad Request", context = {} }) {
    super({ message, statusCode: 400, context });
  }
}

class Unauthorized extends CustomError {
  constructor({ message = "Unauthorized", context = {} }) {
    super({ message, statusCode: 401, context });
  }
}

class Forbidden extends CustomError {
  constructor({ message = "Forbidden", context = {} }) {
    super({ message, statusCode: 403, context });
  }
}

class NotImplemented extends CustomError {
  constructor({ message = "Not Implemented", context = {} }) {
    super({ message, statusCode: 501, context });
  }
}

class ValidationError extends CustomError {
  constructor({ message = "Validation failed", details = [], context = {} }) {
    super({ message, statusCode: 400, context });
    this.details = details;
  }
}

export default { CustomError, InternalServerError, NotFound, BadRequest, Unauthorized, Forbidden, NotImplemented, ValidationError };
