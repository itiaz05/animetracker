import fs from "fs";

// Save data to JSON file
const saveJSON = (name, data) => {
  fs.writeFile(`../../JSONs/${name}.json`, JSON.stringify(data), function (err) {
    if (err) {
      console.log("An error occurred while writing JSON Object to File:");
      console.log(err);
    }
  });
};

export default { saveJSON };
