import { Errors } from "../Shared/index.js";
import validationsHelpers from "./validationsHelpers.js";

const { ValidationError } = Errors;

const capitalizeWords = (str) => {
  const lowerStr = str.toLowerCase();
  return lowerStr
    .split(" ")
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
    .join(" ");
};

const deleteSpecialCharacters = (str) => str.replace(/[^a-zA-Z0-9 ]/g, "");

const convertUnderscoresToSpaces = (str) => str.replace(/_/g, " ");

/**
 * @param {String} input
 * @param {Object} [options]
 * @param {Boolean} [options.replaceUnderscoreWithSpace=false]
 * @param {Boolean} [options.capitalize=false]
 * @param {Boolean} [options.lower=false]
 * @param {Boolean} [options.replaceSpecialsWithUnderscore=true]
 * @param {Boolean} [options.removeSpecialCharacters=false]
 * @returns {String} Returns the cleaned input.
 * @throws {ValidationError}
 */
const cleanInput = (
  input,
  {
    replaceUnderscoreWithSpace = false,
    capitalize = false,
    lower = false,
    replaceSpecialsWithUnderscore = true,
    removeSpecialCharacters = false
  } = {}
) => {
  if (validationsHelpers.isEmpty(input)) {
    throw new ValidationError({ message: "Input is invalid", context: { input } });
  }

  const cleaningMethods = {
    replaceSpecialsWithUnderscore: (str) => str.replace(/[:/]/g, "_"),
    replaceUnderscoreWithSpace: (str) => convertUnderscoresToSpaces(str),
    capitalize: (str) => capitalizeWords(str),
    lower: (str) => str.toLowerCase(),
    removeSpecialCharacters: (str) => deleteSpecialCharacters(str)
  };

  const optionsMap = {
    replaceSpecialsWithUnderscore,
    replaceUnderscoreWithSpace,
    capitalize,
    lower,
    removeSpecialCharacters
  };

  return Object.keys(optionsMap).reduce((result, key) => {
    return optionsMap[key] ? cleaningMethods[key](result) : result;
  }, input);
};

const omitLine = (input, startsWith = "") => {
  if (validationsHelpers.isEmpty(input)) {
    throw new ValidationError({ message: "Input is invalid", context: { input } });
  }

  const lines = input.split("\n");
  const cleanedLines = lines.filter((line) => !line.includes(startsWith));
  return cleanedLines.join("\n").trim();
};

export default { cleanInput, capitalizeWords, deleteSpecialCharacters, convertUnderscoresToSpaces, omitLine };
