import express from "express";
import fs from "fs";
import path from "path";

/**
 * Dynamically combines routers from all subdirectories of a given directory.
 * @param {string} dirPath The path to the directory containing subdirectories with route files.
 * @returns {Promise<express.Router>} The combined router.
 */
const combineRoutersFromDirectory = async (dirPath) => {
  const router = express.Router();

  const subDirs = await fs.promises.readdir(dirPath);

  for (const subDir of subDirs) {
    const subDirPath = path.join(dirPath, subDir);
    const routesPath = path.join(subDirPath, "routes.js");

    try {
      const { default: subDirRouter } = await import(await fs.promises.readFile(routesPath, "utf-8"));

      router.use(`/${subDir}`, subDirRouter);
    } catch (error) {
      console.error(`Error loading routes from ${routesPath}:`, error);
    }
  }

  return router;
};

export default { combineRoutersFromDirectory };
