import bcrypt from "bcrypt";
import { Errors } from "../Shared/index.js";

const { ValidationError } = Errors;

const getSchemaRequiredFields = (schema) => {
  const schemaFields = schema?.paths;
  let requiredFields = [];

  for (const prop in schemaFields) {
    if (schemaFields[prop].isRequired) {
      requiredFields.push(prop);
    }
  }

  return requiredFields;
};

const validateDocument = (document, schema) => {
  const errors = [];
  const schemaRequiredProperties = getSchemaRequiredFields(schema);

  const keys = Object.keys(document);
  for (const field of schemaRequiredProperties) {
    if (!keys.includes(field)) {
      errors.push({ [field]: "Missing required field" });
    }
  }

  if (errors.length > 0) {
    throw new ValidationError({ message: "Failed to validate document", details: errors, context: { document } });
  }
};

const isEmpty = (value) => {
  return (
    !value ||
    (typeof value === "string" && !value.trim().length) ||
    (Array.isArray(value) && !value.length) ||
    (typeof value === "object" && !Object.keys(value).length)
  );
};

/**
 * @param {String} password - The password to be hashed.
 * @param {Number} [saltRounds=10] - Optional number of salt rounds to use. Defaults to 10.
 * @returns {Promise<string>} A promise that resolves to the hashed password.
 */
const hashPassword = async (password, saltRounds = 10) => {
  const salt = await bcrypt.genSalt(saltRounds);
  return await bcrypt.hash(password, salt);
};

/**
 * Compares the provided password with a hashed password.
 * @param {String} password - The password to be compared.
 * @param {String} hashedPassword - The hashed password to compare against.
 * @returns {Promise<boolean>} A promise that resolves to true if the password matches the hashed password, otherwise false.
 */
const isPasswordsEqual = async (password, hashedPassword) => await bcrypt.compare(password, hashedPassword);

export default { validateDocument, isEmpty, hashPassword, isPasswordsEqual };
