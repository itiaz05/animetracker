import { Errors } from "../Shared/index.js";
import validationsHelpers from "./validationsHelpers.js";

const { ValidationError } = Errors;

/**
 * Creates an object composed of the specified properties from the input object.
 * @param {Object} obj - The source object from which properties will be picked.
 * @param {Array<string>} keys - An array of strings representing the keys to pick from the source object.
 * @returns {Object} A new object containing only the specified properties from the source object.
 * @throws {ValidationError} Throws an error if `obj` is not an object.
 *
 * @example
 * const data = { a: 1, b: 2, c: 3 };
 * const picked = pick(data, ['a', 'c']);
 * console.log(picked); // Output: { a: 1, c: 3 }
 */
const pick = (obj, keys) => {
  if (!keys) return obj;
  if (typeof obj !== "object" || validationsHelpers.isEmpty(obj)) {
    throw new ValidationError({ message: "Expected an object", context: { obj, type: typeof obj } });
  }

  return keys.reduce((result, key) => {
    if (obj && obj.hasOwnProperty(key)) {
      result[key] = obj[key];
    }
    return result;
  }, {});
};

/**
 * Creates an object composed of the properties that pass the predicate function.
 * @param {Object} obj - The object to iterate over.
 * @param {Function} predicate - The function invoked for each property.
 * @returns {JSON} The new object with properties that pass the predicate function.
 */
const pickBy = (obj, predicate) => {
  if (!predicate) return obj;
  if (typeof obj !== "object" || validationsHelpers.isEmpty(obj)) {
    throw new ValidationError({ message: "Expected an object", context: { obj, type: typeof obj } });
  }
  if (typeof predicate !== "function") {
    throw new ValidationError({ message: "Expected a predicate function", context: { predicate, type: typeof predicate } });
  }

  return Object.fromEntries(Object.entries(obj).filter(([key, value]) => predicate(value, key)));
};

export default { pick, pickBy };
