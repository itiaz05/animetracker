import express from "express";
import pullRefreshTokenCookies from "../../Middlewares/verifyRefreshToken.js";
import authenticateUser from "../../Middlewares/verifyToken.js";
import controller from "./controller.js";

const router = express.Router();

router.post("/login/", controller.login);
router.post("/logout/", authenticateUser, controller.logout);

router.route("/").post(controller.create).patch(authenticateUser, controller.edit).delete(authenticateUser, controller.deleteUser);

router.get("/refreshToken/", pullRefreshTokenCookies, controller.refreshToken);

export default router;
