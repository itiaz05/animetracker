import mongoose from "mongoose";
import { AuthService } from "../../Services/index.js";
import { validationsHelpers } from "../../Utils/index.js";

const userSchema = new mongoose.Schema({
  username: { type: String, required: true, index: { unique: true } },
  password: { type: String, required: true, minLength: 6 },
  fullName: { type: String, required: true },
  email: {
    type: String,
    required: true,
    unique: true
  }
});

userSchema.pre("save", async function save(next) {
  if (!this.isModified("password")) return next();
  try {
    this.password = await validationsHelpers.hashPassword(this.password);
    return next();
  } catch (err) {
    return next(err);
  }
});

userSchema.pre("delete", async function deleteToken(next) {
  try {
    await AuthService.deleteRefreshToken(this._id);
    return next();
  } catch (err) {
    return next(err);
  }
});

userSchema.methods.validatePassword = async function validatePassword(data) {
  return validationsHelpers.isPasswordsEqual(data, this.password);
};

export default mongoose.model("User", userSchema);
