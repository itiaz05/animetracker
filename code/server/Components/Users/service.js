import { AuthService } from "../../Services/index.js";
import { Errors } from "../../Shared/index.js";
import { objectsHelpers, validationsHelpers } from "../../Utils/index.js";
import AnimeState from "../AnimeState/model.js";
import User from "./model.js";

const { NotFound, BadRequest, ValidationError, Unauthorized, InternalServerError, Forbidden } = Errors;

const create = async (userDetails) => {
  //TODO: replace this with proper validation function
  if (userDetails.password.length < 6) {
    throw new ValidationError({
      message: "Password must be at least 6 characters",
      details: [{ field: "password", message: "Password must be at least 6 characters long", value: password }],
      context: { password }
    });
  }
  validationsHelpers.validateDocument(userDetails, User);

  const user = new User(userDetails);
  user.save();
  return user.username;
};

const login = async ({ username, password }) => {
  if (!username || !password) {
    throw new BadRequest({ message: "Username or Password are invalid", context: { username, password } });
  }

  const user = await User.findOne({ username });
  if (!user) {
    throw new NotFound({
      message: "Failed - User not found",
      context: { username }
    });
  }

  const isPasswordValid = await validationsHelpers.isPasswordsEqual(password, user.password);
  if (!isPasswordValid) {
    throw new Unauthorized({
      message: "Login not successful - Incorrect password",
      context: { username, password }
    });
  }

  return {
    id: user._id,
    username: user.username,
    email: user.email
  };
};

const edit = async ({ id, password, email, fullName }) => {
  if (!id) {
    throw BadRequest({ message: "Id not found", context: { id, email, username } });
  }
  // TODO: validate details
  if (password && password.length < 6) {
    throw ValidationError({
      details: [{ field: "password", message: "Password must be at least 6 characters long", value: password }]
    });
  }

  const fieldsToUpdate = objectsHelpers.pickBy({ password, email, fullName }, (field) => !!field);
  const updated = await User.updateOne({ _id: id }, fieldsToUpdate);
  return updated.acknowledged;
};

const deleteUser = async (userId) => {
  await AnimeState.deleteMany({ userId });
  const isDeleted = await User.findByIdAndDelete(userId);
  return !!isDeleted;
};

export default {
  create,
  login,
  edit,
  deleteUser
};
