import { AuthService, CookiesHandler } from "../../Services/index.js";
import { Errors } from "../../Shared/index.js";
import service from "./service.js";

const { InternalServerError } = Errors;

const login = async (req, res, next) => {
  try {
    const { username, password } = req.body;
    const tokenPayload = await service.login({ username, password });
    const { token, refreshToken } = await AuthService.executeLoginActions(tokenPayload);
    CookiesHandler.setCookie(res, "jwtRT", refreshToken, { maxAge: 60 * 60 * 24 * 7 });
    res.send({ token, username });
  } catch (err) {
    next(err);
  }
};

const logout = async (req, res) => {
  try {
    const userId = req?.user?.id;
    await AuthService.deleteRefreshToken(userId);
    CookiesHandler.clearCookie(res, "jwtRT");
    res.send({ message: "Successfully logged out" });
  } catch (err) {
    next(err);
  }
};

const create = async (req, res, next) => {
  try {
    const { username, password, email, full_name: fullName } = req?.body;
    const dbUsername = await service.create({ username, password, email, fullName });
    if (!dbUsername) {
      throw new InternalServerError({ message: "Failed creating user.", context: { username, password, email, fullName } });
    }

    res.status(201).json(dbUsername);
  } catch (err) {
    next(err);
  }
};

const edit = async (req, res, next) => {
  try {
    const id = req?.user?.id;
    const { fullName, email, password } = req?.body;
    const response = await service.edit({ id, fullName, email, password });
    res.send({ message: response.message });
  } catch (err) {
    next(err);
  }
};

const deleteUser = async (req, res, next) => {
  try {
    const userId = req?.user?.id;
    const isDeleted = await service.deleteUser(userId);
    if (!isDeleted) {
      throw new InternalServerError({ message, context: { userId } });
    }

    res.send({ message: "User deleted successfully." });
  } catch (err) {
    next(err);
  }
};

const refreshToken = async (req, res, next) => {
  try {
    const { user, refreshToken } = req;
    const { token, refreshToken: newRefreshToken } = await AuthService.refreshToken(user, refreshToken);
    CookiesHandler.setCookie(res, "jwtRT", newRefreshToken, { maxAge: 60 * 60 * 24 * 7 });
    res.send({ token });
  } catch (err) {
    next(err);
  }
};

export default {
  login,
  logout,
  create,
  edit,
  deleteUser,
  refreshToken
};
