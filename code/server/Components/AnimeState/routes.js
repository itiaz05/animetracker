import express from "express";
import authenticateUser from "../../Middlewares/verifyToken.js";
import controller from "./controller.js";

const router = express.Router();
const authRouter = express.Router();
authRouter.use(authenticateUser);

router.get("/getListByGenre/:requirements", controller.getListByGenre);
router.get(
  "/getListByRandomGenre/:numberOfResults?", // numberOfResults is optional
  controller.getListByRandomGenre
);

authRouter.get("/myList", controller.getMyList);
authRouter.route("/myList/:id").get(controller.isInList).post(controller.addToList).delete(controller.removeFromList);

authRouter.patch("/updateCurrentEpisode/:animeStateId", controller.updateCurrentEpisode);
authRouter.patch("/updateUserRating/:animeStateId", controller.updateUserRating);
authRouter.patch("/toggleWatched/:animeStateId", controller.toggleIsCompleted);

router.use("/", authRouter);
export default router;
