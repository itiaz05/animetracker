import mongoose from "mongoose";
import { Errors } from "../../Shared/index.js";
import Anime from "../Anime/model.js";
import AnimeState from "./model.js";

const { InternalServerError, BadRequest } = Errors;

const isInList = async ({ userId, animeId }) => {
  return await AnimeState.findOne({ userId, animeId });
};

const addToList = async ({ animeId, userId }) => {
  const existingState = await AnimeState.findOne({ userId, animeId });
  if (existingState) {
    return true;
  }

  await AnimeState.create({ animeId, userId });
  return true;
};

const removeFromList = async ({ animeId, userId }) => {
  return await AnimeState.deleteOne({ userId, animeId });
};

const getMyList = async (userId) => {
  return await AnimeState.find({ userId }).populate("animeId");
};

const updateCurrentEpisode = async (animeStateId, currentEpisode) => {
  const state = await AnimeState.findById(animeStateId);

  if (!state) {
    throw new BadRequest({ message: "Series is not in the list", context: { animeStateId, currentEpisode } });
  }

  if (state?.isCompleted) {
    throw new BadRequest({ message: "Series is completed, can not change current episode", context: { animeStateId, currentEpisode } });
  }

  return await AnimeState.updateOne({ id: animeStateId }, { currentEpisode });
};

const updateUserRating = async (animeStateId, rating) => {
  const state = await AnimeState.findById(animeStateId);
  if (!state) {
    throw { message: "Series is not in the list" };
  }

  state.userRating = rating;
  state.save();
  return { message: "Success", userRating: state.userRating };
};

const toggleIsCompleted = async (animeStateId) => {
  const state = await AnimeState.findById(animeStateId);
  if (!state) {
    throw { message: "Series is not in the list" };
  }

  state.isCompleted = !state.isCompleted;
  if (state.isCompleted) {
    const anime = await Anime.findById(state.animeId);
    if (anime.status === "Finished Airing") state.currentEpisode = anime.numberOfEpisodes;
  }
  state.save();
  return { message: "Success", isCompleted: state.isCompleted };
};

const getAnimeListByGenre = async (genre, numberOfResults = null) => {
  const query = [{ $match: { _id: mongoose.Types.ObjectId } }, { $match: { genres: { $elemMatch: { name: genre } } } }];
  numberOfResults ? query.push({ $limit: +numberOfResults }) : null;
  const list = await Anime.aggregate(query);

  return { genre, list };
};

// Get list of all unique genres
const getAllGenres = async () => {
  const genres = await Anime.distinct("genres.name");
  return { message: "Success", genres };
};

const getRandomGenre = async () => {
  const { genres } = await getAllGenres();
  return genres[Math.floor(Math.random() * genres.length)];
};

export default {
  isInList,
  addToList,
  removeFromList,
  getMyList,
  updateCurrentEpisode,
  updateUserRating,
  toggleIsCompleted,
  getAnimeListByGenre,
  getAllGenres,
  getRandomGenre
};
