import mongoose, { Schema } from "mongoose";

const animeStateSchema = new mongoose.Schema({
  userId: { type: Schema.Types.ObjectId, ref: "User", required: true },
  animeId: { type: Schema.Types.ObjectId, ref: "Anime", required: true },
  currentEpisode: { type: Number, min: 0, default: 0 },
  userRating: { type: Number, min: 0, max: 10 },
  isCompleted: { type: Boolean, default: false },
});

export default mongoose.model("AnimeState", animeStateSchema);
