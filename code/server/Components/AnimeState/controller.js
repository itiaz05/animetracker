import { Errors } from "../../Shared/index.js";
import service from "./service.js";

const { BadRequest, InternalServerError } = Errors;

const controller = {
  // Get anime list by genre
  getListByGenre: async (req, res, next) => {
    try {
      let requirements = req.params?.requirements;
      requirements = JSON.parse(decodeURIComponent(requirements));

      if (!requirements) {
        throw new BadRequest({ message: "No genre provided" });
      }

      const { genre, numberOfResults: limitTo } = requirements;
      const animeList = await service.getAnimeListByGenre(genre, limitTo);

      if (!animeList || !animeList.list.length) {
        throw new InternalServerError({ message: "Failed finding an anime list of that genre.", context: { genre } });
      }

      res.send(animeList);
    } catch (err) {
      next(err);
    }
  },

  // Get random genre anime list
  getListByRandomGenre: async (req, res, next) => {
    try {
      const limitTo = req.params?.numberOfResults;
      const rndGenre = await service.getRandomGenre();
      const animeList = await service.getAnimeListByGenre(rndGenre, limitTo);
      if (!animeList || !animeList?.list?.length) {
        throw new InternalServerError({ message: animeList.message });
      }
      res.send(animeList);
    } catch (err) {
      next(err);
    }
  },

  // Check if anime is in user's list
  isInList: async (req, res, next) => {
    try {
      const animeId = req?.params?.id;
      if (!animeId) {
        throw new BadRequest({ message: "No anime id provided" });
      }

      const userId = req?.user?.id;
      const response = await service.isInList({ userId, animeId });
      if (!response) {
        res.send({ isInList: false });
        return;
      }

      res.send({
        userDetails: response,
        isInList: true
      });
    } catch (err) {
      next(err);
    }
  },

  // Add anime to user's list
  addToList: async (req, res, next) => {
    try {
      const animeId = req?.params?.id;
      if (!animeId) throw new BadRequest({ message: "No anime id provided" });

      const userId = req?.user?.id;
      const inList = await service.addToList({ animeId, userId });

      if (inList) {
        res.status(201).send({ inList });
      }
    } catch (err) {
      next(err);
    }
  },

  // Remove anime from user's list
  removeFromList: async (req, res, next) => {
    try {
      const animeId = req?.params?.id;
      if (!animeId) throw new BadRequest({ message: "No anime id provided" });

      const userId = req?.user?.id;
      const isRemoved = await service.removeFromList({ animeId, userId });

      if (isRemoved) {
        res.send({ message, inList: false });
        return;
      }

      res.send({ message, inList: true });
    } catch (err) {
      next(err);
    }
  },

  // Get user's list
  getMyList: async (req, res, next) => {
    try {
      const userId = req?.user?.id;
      const list = await service.getMyList(userId);

      if (!list || !list.length) {
        throw new InternalServerError({ message, context: { userId } });
      }

      res.send({ list, count: list.length });
    } catch (err) {
      next(err);
    }
  },

  // Update current episode
  updateCurrentEpisode: async (req, res, next) => {
    try {
      const animeStateId = req?.params?.animeStateId;
      const currentEpisode = req?.body?.episode;

      if (!animeStateId || !currentEpisode) {
        throw new BadRequest({
          message: "Invalid episode number provided",
          context: { animeStateId, currentEpisode }
        });
      }

      //TODO: fix data syncing issue
      const updatedDoc = await service.updateCurrentEpisode(animeStateId, currentEpisode);
      if (!updatedDoc) {
        throw new InternalServerError({ message: "Failed to update episode.", context: { animeStateId, currentEpisode } });
      }
      // TODO: improve response structure
      res.send({ success: true, currentEpisode: updatedDoc.currentEpisode });
    } catch (err) {
      next(err);
    }
  },

  // Update user rating
  updateUserRating: async (req, res, next) => {
    try {
      const animeStateId = req.params.animeStateId;
      const rating = req.body.rating;

      if (!animeStateId || !rating) {
        throw { message: "Invalid details provided" };
      }

      const response = await service.updateUserRating(animeStateId, rating);
      if (response?.message !== "Success") {
        throw {
          message: `Error updating user rating: ${response.message}`
        };
      }

      res.send({
        message: "User rating updated",
        userRating: response.userRating
      });
    } catch (err) {
      next(err);
    }
  },

  // Toggle isCompleted
  toggleIsCompleted: async (req, res, next) => {
    try {
      const animeStateId = req?.params?.animeStateId;
      if (!animeStateId) {
        throw { message: "Invalid details provided" };
      }

      const response = await service.toggleIsCompleted(animeStateId);
      if (response?.message !== "Success") {
        throw { message: response.message };
      }

      res.send({
        message: "Watched status updated",
        isCompleted: response.isCompleted
      });
    } catch (err) {
      next(err);
    }
  }
};

export default controller;
