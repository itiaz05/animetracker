import { InformationFetcher } from "../../Services/index.js";
import { Errors } from "../../Shared/index.js";
import { stringsHelpers, validationsHelpers } from "../../Utils/index.js";
import Anime from "./model.js";

const { BadRequest, NotFound } = Errors;

const cleanStringFields = async (fullAnimeDetails, fieldsToClean) => {
  if (!fieldsToClean) return fullAnimeDetails;
  if (typeof fullAnimeDetails !== "object" || validationsHelpers.isEmpty(fullAnimeDetails)) {
    throw new ValidationError({ message: "Expected an object", context: { fullAnimeDetails, type: typeof fullAnimeDetails } });
  }

  const cleanedFieldsAnime = structuredClone(
    fullAnimeDetails?._doc ? { ...fullAnimeDetails.toObject(), _id: fullAnimeDetails.id } : fullAnimeDetails
  );

  for (const field in fieldsToClean) {
    cleanedFieldsAnime[field] = stringsHelpers.cleanInput(fullAnimeDetails[field], fieldsToClean[field]);
  }

  const description = stringsHelpers.omitLine(fullAnimeDetails?.description, "[Written");
  cleanedFieldsAnime.description = description;

  return cleanedFieldsAnime;
};

const saveAnime = async (animeDetails) => {
  if (!animeDetails) {
    throw new BadRequest({ message: "Anime details are empty" });
  }
  validationsHelpers.validateDocument(animeDetails, Anime.schema);

  const cleanedAnime = await cleanStringFields(animeDetails, {
    title: { capitalize: true },
    status: { replaceUnderscoreWithSpace: true, capitalize: true },
    alternativeTitle: { capitalize: true }
  });
  const anime = new Anime(cleanedAnime);
  anime.save();
  return anime;
};

// Get anime from db by name
const getAnime = async (animeName) => {
  if (!animeName) {
    throw new BadRequest({ message: "Anime name is empty" });
  }

  const cleanedInput = stringsHelpers.cleanInput(animeName, { capitalize: true });
  const anime = await Anime.findOne({ $or: [{ title: cleanedInput }, { alternativeTitle: cleanedInput }] });
  if (!anime) {
    return null;
  }

  const cleanedAnime = await cleanStringFields(anime, {
    title: { replaceUnderscoreWithSpace: true, capitalize: true },
    alternativeTitle: { replaceUnderscoreWithSpace: true, capitalize: true }
  });
  return cleanedAnime;
};

const getAnimeFromExternalProvider = async (searchTerm) => {
  const details = await InformationFetcher.getAnimeDetails(searchTerm);
  if (!details) {
    throw new NotFound({ message: "No details found", context: { searchTerm } });
  }

  const savedAnime = await saveAnime(details);
  if (savedAnime) {
    console.log(`Anime '${savedAnime.title}' saved to db`);
    const cleanedAnime = await cleanStringFields(savedAnime, {
      title: { replaceUnderscoreWithSpace: true, capitalize: true },
      alternativeTitle: { replaceUnderscoreWithSpace: true, capitalize: true }
    });
    return cleanedAnime;
  }
};

export default { saveAnime, getAnime, getAnimeFromExternalProvider };
