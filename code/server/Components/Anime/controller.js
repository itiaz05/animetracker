import service from "./service.js";

// Search for anime at db or provider
const searchFor = async (req, res, next) => {
  const searchTerm = req?.params?.term;
  try {
    const anime = await service.getAnime(searchTerm);

    if (anime) {
      console.log(`Anime '${anime.title}' found on db`);
      res.send(anime);
      return;
    }
  } catch (error) {
    console.log(error);
  }

  try {
    const animeFromProvider = await service.getAnimeFromExternalProvider(searchTerm);
    res.send(animeFromProvider);
  } catch (error) {
    next(err)
  }
};

export default { searchFor };
