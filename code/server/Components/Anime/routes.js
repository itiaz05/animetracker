import express from "express";
import controller from "./controller.js";

const router = express.Router();

router.get("/searchFor/:term", controller.searchFor);

export default router;
