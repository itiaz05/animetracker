import mongoose from "mongoose";
import AnimeState from "../AnimeState/model.js";

const animeSchema = new mongoose.Schema({
  title: { type: String, required: true, unique: true },
  alternativeTitle: String,
  year: Number,
  numberOfEpisodes: Number,
  description: String,
  rating: { type: Number, min: 0, max: 10 },
  image: String,
  status: String,
  genres: {
    type: [Object],
    properties: {
      id: Number,
      name: String,
    },
  },
});

animeSchema.pre("remove", function (next) {
  const anime = this;
  AnimeState.remove({ animeId: anime._id }).exec();
  next();
});

export default mongoose.model("Anime", animeSchema);
