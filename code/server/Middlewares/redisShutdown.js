import { RedisHandler } from "../Services/index.js";

const redisShutdown = (server) => {
  const gracefulShutdown = async () => {
    console.log("Shutting down Redis client...");
    try {
      await RedisHandler.disconnectRedis();
      console.log("Redis client shut down successfully");
    } catch (error) {
      console.error("Error shutting down Redis client:", error);
    } finally {
      server.close(() => {
        console.log("Express server closed");
        process.exit(0);
      });
    }
  };

  process.on("SIGTERM", gracefulShutdown);
  process.on("SIGINT", gracefulShutdown);
};

export default redisShutdown;
