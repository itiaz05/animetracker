import { Errors } from "../Shared/index.js";

const { CustomError } = Errors;

const errorHandler = async (err, req, res, next) => {
  console.error(err.stack); // can later use logging tools

  if (err && err instanceof CustomError) {
    const { statusCode, message, context, details } = err;
    return res.status(statusCode).json({ message, context, details: details?.length ? details : [] });
  }

  // Handle non-custom errors
  return res.status(500).json({ message: "Internal Server Error" });
};

export default errorHandler;
