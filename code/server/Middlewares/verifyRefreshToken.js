import { CookiesHandler, JwtHandler } from "../Services/index.js";

const verifyRefreshToken = async (req, res, next) => {
  const jwtCookie = req.cookies?.jwtRT;
  if (!jwtCookie) {
    CookiesHandler.clearCookie(res, "jwtRT");
    return res.sendStatus(401);
  }

  try {
    const user = await JwtHandler.decodeToken(jwtCookie, true);
    if (!user) {
      CookiesHandler.clearCookie(res, "jwtRT");
      return res.sendStatus(403);
    }

    req.user = user;
    req.refreshToken = jwtCookie;
    next();
  } catch (error) {
    console.error("Error in verifyRefreshToken: ", error);
    return res.sendStatus(403);
  }
};

export default verifyRefreshToken;
