import { ThemeProvider } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import { useEffect, useState } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Footer from "./Components/Shared/Footer/footer";
import Navbar from "./Components/Shared/Navbar/navbar";
import AxiosAuth from "./Services/API/axiosAuth";
import Routing from "./Services/Routing/routing";
import darkTheme from "./Theme/dark";
import lightTheme from "./Theme/light";
import ThemeButton from "./Theme/themeButton";

function App() {
  const Axios = AxiosAuth(); //just for enable axios interceptors
  const themeFromStorage = localStorage.getItem("theme") || "light";
  const initialTheme = themeFromStorage === "light" ? lightTheme : darkTheme;
  const [theme, setTheme] = useState(initialTheme);

  useEffect(() => {
    localStorage.setItem("theme", theme.palette.mode);
  }, [theme]);

  const toggleTheme = () => {
    setTheme(theme.palette?.mode === "light" ? darkTheme : lightTheme);
  };

  return (
    <Router>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="App">
          <Navbar>
            <ThemeButton theme={theme} onClick={toggleTheme} />
          </Navbar>
          <Routing />
          <Footer />
        </div>
      </ThemeProvider>
    </Router>
  );
}

export default App;
