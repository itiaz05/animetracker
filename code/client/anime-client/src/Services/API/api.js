import serverAxios from "./axiosInstance";

class SeriesService {
  // Search for anime by name (db and provider)
  static searchFor(searchTerm) {
    if (!searchTerm || searchTerm === "") return "invalid input";
    searchTerm = searchTerm.replace(/\s+/g, " ").toLowerCase();
    return serverAxios({
      method: "GET",
      url: `/api/anime/searchFor/${searchTerm}`
    }).then((response) => response.data);
  }

  // Check if anime is in my list
  static async isInList(animeId) {
    const response = await serverAxios({
      method: "GET",
      url: `/api/animeState/myList/${animeId}`
    });
    return response.data;
  }

  // Add anime to my list
  static async addToList(animeId) {
    const response = await serverAxios({
      method: "POST",
      url: `/api/animeState/myList/${animeId}`
    });
    return response.data;
  }

  // Remove anime from my list
  static async removeFromList(animeId) {
    const response = await serverAxios({
      method: "DELETE",
      url: `/api/animeState/myList/${animeId}`
    });
    return response.data;
  }

  // Get My list
  static async getMyList() {
    const response = await serverAxios({
      method: "GET",
      url: "/api/animeState/myList"
    });
    return response.data;
  }

  // Update current episode
  static async updateCurrentEpisode(animeStateId, episode) {
    const episodeToUpdate = new FormData();
    episodeToUpdate["episode"] = episode;

    const response = await serverAxios({
      method: "PATCH",
      url: `/api/animeState/updateCurrentEpisode/${animeStateId}`,
      data: JSON.stringify(episodeToUpdate)
    });
    return response.data;
  }

  // Update user rating
  static async updateRating(animeStateId, rating) {
    const ratingToUpdate = new FormData();
    ratingToUpdate["rating"] = rating;

    const response = await serverAxios({
      method: "PATCH",
      url: `/api/animeState/updateUserRating/${animeStateId}`,
      data: JSON.stringify(ratingToUpdate)
    });
    return response.data;
  }

  // Toggle isCompleted
  static async toggleIsCompleted(animeStateId) {
    const response = await serverAxios({
      method: "PATCH",
      url: `/api/animeState/toggleWatched/${animeStateId}`
    });
    return response.data;
  }

  // Get list by genre
  static async getListByGenre(genre, numberOfResults = null) {
    const requirements = { genre };
    if (numberOfResults) {
      requirements.numberOfResults = numberOfResults;
    }

    const response = await serverAxios({
      method: "GET",
      url: `/api/animeState/getListByGenre/${encodeURIComponent(JSON.stringify(requirements))}`
    });
    return response.data;
  }

  // Get list by random genre
  static async getListByRandomGenre(numberOfResults = null) {
    const response = await serverAxios({
      method: "GET",
      url: `/api/animeState/getListByRandomGenre/${numberOfResults ? numberOfResults : ""}`
    });
    return response.data;
  }
}

class UsersService {
  // Create new user
  static async createUser(details) {
    const userDetails = new FormData();

    Object.keys(details).forEach((key) => {
      userDetails[key] = details[key];
    });

    const response = await serverAxios({
      method: "POST",
      url: "users/",
      data: JSON.stringify(userDetails)
    });
    return response.data;
  }

  // Login user
  static async login(details) {
    const userDetails = new FormData();
    Object.keys(details).forEach((key) => {
      userDetails[key] = details[key];
    });

    const response = await serverAxios({
      method: "POST",
      url: "users/login/",
      data: JSON.stringify(userDetails)
    });
    return response.data;
  }

  // Logout
  static async logout() {
    const response = await serverAxios({
      method: "POST",
      url: "users/logout/"
    });

    delete serverAxios.defaults.headers.common["Authorization"];
    return response.data;
  }

  // Edit user details
  static async editUser(details) {
    const userDetails = new FormData();
    Object.keys(details).forEach((key) => {
      userDetails[key] = details[key];
    });

    return await serverAxios({
      method: "PATCH",
      url: "users/",
      data: JSON.stringify(userDetails)
    });
  }

  // Delete user
  static async deleteUser() {
    return await serverAxios({
      method: "DELETE",
      url: "users/"
    });
  }

  // Refresh token
  static async refreshToken() {
    const response = await serverAxios({
      method: "GET",
      url: "users/refreshToken/",
      withCredentials: true
    });
    return response.data;
  }
}

const apiService = { SeriesService, UsersService };
export default apiService;
