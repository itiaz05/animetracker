// import {
//   createContext,
//   useCallback,
//   useContext,
//   useEffect,
//   useRef,
// } from "react";
// import { useNavigate } from "react-router-dom";
// import apiService from "../API/api";

// const AuthContext = createContext();

// const AuthProvider = ({ children }) => {
//   const navigate = useNavigate();
//   const isAuthenticatedRef = useRef(false);
//   const isAuthenticated = isAuthenticatedRef.current;

//   const setIsAuthenticated = useCallback((val) => {
//     isAuthenticatedRef.current = val;
//   }, []);

//   useEffect(() => {
//     const userDetails = localStorage.getItem("userDetails");
//     const accessToken = localStorage.getItem("accessSecret");
//     // TODO: validate token
//     if (accessToken && userDetails) {
//       setIsAuthenticated(true);
//     } else {
//       setIsAuthenticated(false);
//     }
//   }, [setIsAuthenticated]);

//   const logoutBrowser = useCallback(() => {
//     setIsAuthenticated(false);
//     localStorage.removeItem("userDetails");
//     localStorage.removeItem("accessSecret");
//     navigate("/");
//     return true;
//   }, [setIsAuthenticated, navigate]);

//   const loginUser = useCallback((userDetails, token) => {
//     localStorage.setItem("userDetails", JSON.stringify(userDetails));
//     localStorage.setItem("accessSecret", token);
//   }, []);

//   const logout = useCallback(() => {
//     apiService.UsersService.logout()
//       .catch((error) => {
//         console.log(error);
//       })
//       .finally(() => {
//         return logoutBrowser();
//       });
//   }, [logoutBrowser]);

//   return (
//     <AuthContext.Provider
//       value={{
//         isAuthenticated,
//         setIsAuthenticated,
//         logout,
//         logoutBrowser,
//         loginUser,
//       }}
//     >
//       {children}
//     </AuthContext.Provider>
//   );
// };

// const useAuth = () => useContext(AuthContext);

// export { AuthProvider, useAuth };
