import { Route, Routes } from "react-router-dom";
import About from "../../Components/About/about";
import AnimePage from "../../Components/AnimePage/animePage.js";
import Login from "../../Components/Auth/login";
import SignUp from "../../Components/Auth/signUp";
import Home from "../../Components/Homepage/home.js";
import NotFound from "../../Components/NotFound/notFound";
import PersistLogin from "../../Components/PersistLogin/persistLogin";
import RequireAuth from "../../Components/RequireAuth/requireAuth";
import ListPage from "../../Components/Shared/ListPage/listPage";
import UserSettings from "../../Components/UserSettings/userSettings";

const Routing = () => {
  return (
    <Routes>
      <Route element={<PersistLogin />}>
      {/* Public Routes */}
      <Route exact path="/" element={<Home />}></Route>
      <Route exact path="/login" element={<Login />}></Route>
      <Route exact path="/signup" element={<SignUp />}></Route>
      <Route exact path="/about" element={<About />}></Route>
      <Route exact path="/animePage/:title" element={<AnimePage />}></Route>
      <Route exact path="/listPage/:genre" element={<ListPage />}></Route>

      {/* Authenticated Routes */}
        <Route element={<RequireAuth />}>
          <Route exact path="/settings" element={<UserSettings />}></Route>
          <Route exact path="/myList" element={<ListPage />}></Route>
        </Route>
      </Route>

      {/* Default Route */}
      <Route path="*" element={<NotFound />}></Route>
    </Routes>
  );
};

export default Routing;
