import { useEffect, useState } from "react";
import apiService from "../../Services/API/api";
import AnimeList from "../Shared/AnimeList/animeList";
import style from "./home.module.css";

const Home = () => {
  const [seriesList, setSeriesList] = useState([]);
  const [isError, setIsError] = useState(false);
  const [genreDisplay, setGenreDisplay] = useState("");
  const showList = () => seriesList?.length > 0;

  useEffect(() => {
    try {
      apiService.SeriesService.getListByRandomGenre(9).then((response) => {
        setSeriesList(response.list);
        setGenreDisplay(response.genre);
      });
      showList();
    } catch (error) {
      setIsError(true);
      console.log(error);
    }
  }, []);

  return (
    <div className={style.home}>
      <h1 className={style.main_title}> Welcome To Home Page</h1>

      {!isError && showList && (
        <AnimeList
          className={style.genre_list}
          list={seriesList}
          cardSizes={{ cardsHeight: "200px", cardsWidth: "200px" }}
          genre={genreDisplay}
        />
      )}
    </div>
  );
};

export default Home;
