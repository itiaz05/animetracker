import FavoriteSharpIcon from "@mui/icons-material/FavoriteSharp";
import { Checkbox } from "@mui/material";
import Button from "@mui/material/Button";
import Input from "@mui/material/Input";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import Slider from "@mui/material/Slider";
import TextField from "@mui/material/TextField";
import ToggleButton from "@mui/material/ToggleButton";
import Typography from "@mui/material/Typography";
import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import apiService from "../../Services/API/api";
import { useAuth } from "../../Services/Auth/authProvider";
import useGenres from "../../Services/Hooks/useGenres";
import AlertDialog from "../Shared/alertDialog";
import style from "./animePage.module.css";

const AnimePage = () => {
  const { state } = useLocation();
  const location = useLocation();
  const [currEpisode, setCurrEpisode] = useState(-1);
  const navigate = useNavigate();
  const [userRating, setUserRating] = useState(0.0);
  const [details, setDetails] = useState({});
  const [userDetails, setUserDetails] = useState({});
  const [inList, setInList] = useState(false);
  const [showDialog, setShowDialog] = useState(false);
  const [showDialogProgress, setShowDialogProgress] = useState(true);
  const [textMessage, setTextMessage] = useState("Error");
  const [dialogTitle, setDialogTitle] = useState("Error");
  const [validationError, setValidationError] = useState(false);
  const [myListHover, setMyListHover] = useState(false);
  const { auth } = useAuth();
  const { destructureGenres } = useGenres();

  useEffect(() => {
    if (state === undefined || state === null) {
      navigate("/not-found");
    }
    const genresToSet = destructureGenres(state["genres"]);
    setDetails({ ...state, genres: genresToSet });
  }, [state, destructureGenres]);

  useEffect(() => {
    if (auth?.user) {
      if (details && details["_id"] !== undefined) {
        apiService.SeriesService.isInList(details["_id"])
          .then((response) => {
            if (response["isInList"]) {
              setUserDetails(response["userDetails"]);
              response["userDetails"]["userRating"] !== undefined ? setUserRating(response["userDetails"]["userRating"]) : setUserRating(0);
              setCurrEpisode(0);
              if (response["userDetails"]["currentEpisode"]) setCurrEpisode(response["userDetails"]["currentEpisode"]);
            }
            setInList(response["isInList"]);
          })
          .catch((error) => {
            setInList(false);
          });
      }
    }
  }, [auth, details, inList]);

  const handleFavoriteClick = () => {
    setDialogTitle("Favorite");
    setShowDialog(true);
    setShowDialogProgress(true);
    if (inList) {
      apiService.SeriesService.removeFromList(details["_id"])
        .then((response) => {
          setShowDialogProgress(false);
          if (response["message"] === "Success") {
            setTextMessage("Series removed from your list");
            setInList(response["inList"]);
          } else {
            setTextMessage(response.message);
            setValidationError(true);
          }
        })
        .catch((error) => {
          setShowDialogProgress(false);
          setTextMessage(`${error?.message}: ${error?.response?.data?.message}`);
          if (error.response.status === 401) {
            setValidationError(true);
          }
        });
    } else {
      apiService.SeriesService.addToList(details["_id"])
        .then((response) => {
          setShowDialogProgress(false);
          if (response["message"] === "Success") {
            setTextMessage("Series Added to your list");
            setInList(response["inList"]);
          }
        })
        .catch((error) => {
          // console.log(error);
          setShowDialogProgress(false);
          setTextMessage(`${error.message}: ${error.response.data.message}`);
          if (error.response.status === 401) {
            setValidationError(true);
          }
        });
    }
  };

  const handleEpisodeChange = (event) => {
    setCurrEpisode(event.target.value);
  };

  const updateCurrentEpisode = () => {
    setDialogTitle("Update Current Episode");
    setShowDialog(true);
    setShowDialogProgress(true);
    if (currEpisode < 0) {
      setShowDialogProgress(false);
      setTextMessage("Error: Current episode must be a positive number");
      return;
    }
    apiService.SeriesService.updateCurrentEpisode(userDetails["_id"], currEpisode)
      .then((response) => {
        setShowDialogProgress(false);
        setTextMessage(`Current episode updated to ${response?.currentEpisode}`);
      })
      .catch((error) => {
        // console.log(error);
        setShowDialogProgress(false);
        setTextMessage(`${error.message}: ${error.response.data.message}`);
      });
  };

  const handleSliderChange = (_event, newValue) => {
    setUserRating(newValue);
  };

  const handleRatingChange = (event) => {
    setUserRating(event.target.value === 0 ? 0 : Number(event.target.value));
  };

  const updateRating = () => {
    setDialogTitle("Update Rating");
    setShowDialog(true);
    setShowDialogProgress(true);
    if (userRating < 0 || userRating > 10) {
      setShowDialogProgress(false);
      setTextMessage("Error: Rating must be between 0 and 10");
      return;
    }
    apiService.SeriesService.updateRating(userDetails["_id"], userRating === 0 ? "0" : userRating)
      .then((response) => {
        setShowDialogProgress(false);
        if (response["message"] === "User rating updated") {
          setTextMessage(`Rating updated to ${response["userRating"]}`);
          setUserRating(+response["userRating"]); // + converts string to number
        }
      })
      .catch((error) => {
        console.log(error);
        setShowDialogProgress(false);
        setTextMessage(`${error.message}: ${error.response.data.message}`);
      });
  };

  const closeAlertHandler = () => {
    setShowDialog(false);
  };

  const closeAlertHandlerRedirect = () => {
    setShowDialog(false);
    setValidationError(false);
    navigate("/login", { state: { from: location.pathname }, replace: true });
  };

  const handleCompletedClick = () => {
    setDialogTitle("Completed");
    setShowDialog(true);
    setShowDialogProgress(true);
    apiService.SeriesService.toggleIsCompleted(userDetails["_id"])
      .then((response) => {
        setShowDialogProgress(false);
        setTextMessage(response["message"]);
        setDetails((prevState) => ({
          ...prevState,
          isCompleted: response["isCompleted"]
        }));
        setCurrEpisode(+details["numOfEpisodes"]);
        setUserDetails((prevState) => ({
          ...prevState,
          currentEpisode: +details["numOfEpisodes"]
        }));
      })
      .catch((error) => {
        console.log(error);
        setShowDialogProgress(false);
        setTextMessage(`${error?.message}: ${error?.response?.data?.message}`);
      });
  };

  return (
    <div className={style.anime_page}>
      <h1 className={style.header}>{details["title"]}</h1>

      <div className={style.page_body}>
        <div className={style.details}>
          <Typography variant="h4" component="div">
            Series Details:
          </Typography>
          <List className={style.details_list}>
            <ListItem>
              <ListItemText primary="Alternative Title:" secondary={details["alternativeTitle"]} />
            </ListItem>
            <ListItem>
              <ListItemText primary="Year:" secondary={details["year"]} />
            </ListItem>
            <ListItem>
              <ListItemText primary="Status:" secondary={details["status"]} />
            </ListItem>
            <ListItem>
              <ListItemText
                primary="Number of episodes:"
                secondary={details["numberOfEpisodes"] === 0 ? "Unknown" : details["numberOfEpisodes"]}
              />
            </ListItem>
            <ListItem>
              <ListItemText primary="Rating:" secondary={details["rating"]} />
            </ListItem>
            {details["genres"] && (
              <ListItem>
                <ListItemText primary="Genres:" secondary={details["genres"].join(", ")} />
              </ListItem>
            )}
          </List>
        </div>

        <div className={style.user_details}>
          {inList ? (
            <div className={style.user_details}>
              <Typography variant="h4" component="div">
                User Details:
              </Typography>
              <div className={style.user_rating}>
                <div className={style.rating_slider}>
                  <Typography id="input-slider">Your Rating</Typography>
                  <Slider
                    aria-label="Always visible"
                    defaultValue={userRating}
                    value={userRating}
                    onChange={handleSliderChange}
                    step={0.1}
                    min={0.0}
                    max={10}
                    valueLabelDisplay="auto"
                  />
                </div>
                <Input
                  className={style.rating_input}
                  type="number"
                  placeholder={`${userRating}`}
                  onChange={handleRatingChange}
                  value={userRating}
                />
                <Button variant="contained" onClick={updateRating}>
                  Update
                </Button>
              </div>

              <div>
                <Typography id="input-number">Current Episode</Typography>
                <Input
                  type="number"
                  placeholder={userDetails["currentEpisode"] ? `${userDetails["currentEpisode"]}` : "Your Current Episode"}
                  onChange={handleEpisodeChange}
                  disabled={userDetails["isCompleted"]}
                />
                <Button variant="contained" disabled={userDetails["isCompleted"]} onClick={updateCurrentEpisode}>
                  Update episode
                </Button>
              </div>

              <div className={style.completedContainer}>
                <Typography id="input-boolean">Is Completed?</Typography>
                <Checkbox
                  value="completed"
                  aria-label="completed"
                  checked={userDetails["isCompleted"] || false}
                  onChange={handleCompletedClick}
                  size="medium"
                ></Checkbox>
              </div>
            </div>
          ) : null}
        </div>

        <div className={style.image}>
          <div>
            <img src={details["image"]} alt="Anime" />
          </div>
          <div onPointerEnter={() => setMyListHover(true)} onPointerLeave={() => setMyListHover(false)} className={style.details_fav_icon}>
            {inList ? <span>Remove from 'my list' </span> : <span>Add to 'my list' </span>}
            <ToggleButton
              value="favorite"
              aria-label="like"
              selected={inList}
              disabled={auth?.user ? false : true}
              onChange={handleFavoriteClick}
              size="medium"
            >
              <FavoriteSharpIcon color={inList ? "myList" : "notMyList"} />
            </ToggleButton>
          </div>
          {!auth?.user && myListHover && (
            <Typography variant="h6" component="div" color="warning.main">
              You have to login first!
            </Typography>
          )}
        </div>
      </div>
      <TextField
        className={style.details_description}
        label="Description"
        multiline
        value={details["description"]}
        variant="outlined"
        InputProps={{
          readOnly: true,
          inputmultiline: "true",
          autoFocus: true
        }}
        InputLabelProps={{
          sx: { color: "secondary.text", fontSize: "1rem", shrink: true }
        }}
      />
      <AlertDialog
        showDialog={showDialog}
        showDialogProgress={showDialogProgress}
        closeHandler={closeAlertHandler}
        closeHandlerRedirect={closeAlertHandlerRedirect}
        redirectMessage="Go To Login"
        showRedirectButton={validationError}
        title={dialogTitle}
        description={textMessage}
      />
    </div>
  );
};

export default AnimePage;
