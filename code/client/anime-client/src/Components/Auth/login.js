import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { styled } from "@mui/material/styles";
import { createRef, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import apiService from "../../Services/API/api";
import { useAuth } from "../../Services/Auth/authProvider";
import AlertDialog from "../Shared/alertDialog";
import style from "./auth.module.css";

const Login = () => {
  const { setAuth } = useAuth();
  const username = createRef();
  const password = createRef();
  const navigate = useNavigate();
  const location = useLocation();
  const from = location.state?.from?.pathname || "/";

  const [showDialog, setShowDialog] = useState(false);
  const [showDialogProgress, setShowDialogProgress] = useState(false);
  const [dialogTitle, setDialogTitle] = useState("Login");
  const [textMessage, setTextMessage] = useState("");
  const [validationError, setValidationError] = useState(false);

  const ValidationTextField = styled(TextField)({
    "& input:valid + fieldset": {
      borderColor: "green",
      borderWidth: 2
    },
    "& input:invalid + fieldset": {
      borderColor: "red",
      borderWidth: 2
    },
    "& input:valid:focus + fieldset": {
      borderLeftWidth: 6,
      padding: "4px !important" // override inline-style
    }
  });

  const handleLogin = (e) => {
    e.preventDefault();
    setDialogTitle("Login");
    setShowDialogProgress(true);
    setShowDialog(true);
    const formValues = {};
    formValues["username"] = username.current.value;
    formValues["password"] = password.current.value;
    apiService.UsersService.login(formValues)
      .then((response) => {
        const token = response["token"] || null;
        if (token) {
          setTextMessage("Success");
          setShowDialogProgress(false);
          const usernameToSave = response.username.toString();
          setAuth({
            user: usernameToSave,
            token: token
          });
          localStorage.setItem("MAT-U-N", JSON.stringify(usernameToSave));
        } else {
          setDialogTitle("Login Failed");
          setValidationError(true);
          setTextMessage(`Error: ${response["message"]}`);
          setShowDialogProgress(false);
        }
      })
      .catch((error) => {
        setDialogTitle("Login Failed");
        setValidationError(true);
        setTextMessage(error?.response?.data?.message);
        setShowDialogProgress(false);
      });
  };

  const closeAlertHandler = () => {
    setShowDialog(false);
    if (!validationError) {
      navigate(from, { replace: true });
    }
    setValidationError(false);
  };

  const closeAlertHandlerRedirect = () => {
    setShowDialog(false);
    setValidationError(false);
    navigate("/signup");
  };

  return (
    <Box component="form" noValidate className={style.container}>
      <ValidationTextField
        label="Username"
        name="username"
        required
        variant="outlined"
        id="validation-username-input"
        inputRef={username}
      />
      <ValidationTextField
        label="Password"
        name="password"
        type="password"
        required
        variant="outlined"
        id="validation-pw-input"
        inputRef={password}
      />
      <Button className={style.signButton} variant="contained" type="submit" onClick={handleLogin}>
        Login
      </Button>
      <div>
        Don't have a user?
        <Button onClick={closeAlertHandlerRedirect}>Sign-Up</Button>
      </div>
      <AlertDialog
        showDialog={showDialog}
        showDialogProgress={showDialogProgress}
        closeHandler={closeAlertHandler}
        closeHandlerRedirect={closeAlertHandlerRedirect}
        redirectMessage="Go To Sign-Up"
        showRedirectButton={validationError}
        title={dialogTitle}
        description={textMessage}
      />
    </Box>
  );
};

export default Login;
