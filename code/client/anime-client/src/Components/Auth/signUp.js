import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import TextField from "@mui/material/TextField";
import { createRef, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import apiService from "../../Services/API/api";
import AlertDialog from "../Shared/alertDialog";
import style from "./auth.module.css";

const SignUp = () => {
  const username = createRef();
  const password = createRef();
  const email = createRef();
  const full_name = createRef();
  const [showDialog, setShowDialog] = useState(false);
  const [showDialogProgress, setShowDialogProgress] = useState(false);
  const [dialogTitle, setDialogTitle] = useState("Sign Up");
  const [textMessage, setTextMessage] = useState("");
  const [validationError, setValidationError] = useState(false);
  const navigate = useNavigate();

  const ValidationTextField = styled(TextField)({
    "& input:valid + fieldset": {
      borderColor: "green",
      borderWidth: 2,
    },
    "& input:invalid + fieldset": {
      borderColor: "red",
      borderWidth: 2,
    },
    "& input:valid:focus + fieldset": {
      borderLeftWidth: 6,
      padding: "4px !important", // override inline-style
    },
  });

  const handleRegister = () => {
    setDialogTitle("Sign Up");
    setShowDialogProgress(true);
    setShowDialog(true);
    const formValues = {};
    formValues["username"] = username.current.value;
    formValues["password"] = password.current.value;
    formValues["email"] = email.current.value;
    formValues["full_name"] = full_name.current.value;
    apiService.UsersService.createUser(formValues)
      .then((response) => {
        if (response.message === "User created successfully") {
          setTextMessage(response.message);
          setShowDialogProgress(false);
        }
        console.log(response);
      })
      .catch((error) => {
        setDialogTitle("Sign Up Failed");
        setValidationError(true);
        setTextMessage(error.response.data.message);
        setShowDialogProgress(false);
      });
  };

  const closeAlertHandler = () => {
    setShowDialog(false);
    if (!validationError) {
      navigate("/login");
    }
    setValidationError(false);
  };

  const closeAlertHandlerRedirect = () => {
    setShowDialog(false);
    setValidationError(false);
    navigate("/login");
  };

  return (
    <Box component="form" noValidate className={style.container}>
      <ValidationTextField
        className="inputField"
        label="Full Name"
        name="full_name"
        required
        variant="outlined"
        id="validation-pw-input"
        inputRef={full_name}
      />
      <ValidationTextField
        label="Username"
        name="username"
        required
        variant="outlined"
        id="validation-username-input"
        inputRef={username}
      />
      <ValidationTextField
        label="Password"
        name="password"
        required
        variant="outlined"
        id="validation-pw-input"
        inputRef={password}
      />
      <ValidationTextField
        label="E-mail"
        name="email"
        required
        variant="outlined"
        id="validation-email-input"
        inputRef={email}
      />
      <Button
        className="signButton"
        variant="contained"
        onClick={handleRegister}
      >
        Sign Up
      </Button>
      <div>
        Already Registered? <Button onClick={closeAlertHandler}>Sign-In</Button>
      </div>
      <AlertDialog
        showDialog={showDialog}
        showDialogProgress={showDialogProgress}
        closeHandler={closeAlertHandler}
        closeHandlerRedirect={closeAlertHandlerRedirect}
        redirectMessage="Go To Login"
        showRedirectButton={validationError}
        title={dialogTitle}
        description={textMessage}
      />
    </Box>
  );
};

export default SignUp;
