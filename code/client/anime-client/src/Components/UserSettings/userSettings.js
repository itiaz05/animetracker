import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import { createRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import apiService from "../../Services/API/api";
import AlertDialog from "../Shared/alertDialog";
import DeleteButton from "./deleteButton";
import style from "./userSettings.module.css";

const UserSettings = () => {
  const fullName = createRef();
  const email = createRef();
  const password = createRef();
  const navigate = useNavigate();
  const [showDialog, setShowDialog] = useState(false);
  const [showDialogProgress, setShowDialogProgress] = useState(false);
  const [dialogTitle, setDialogTitle] = useState("Settings");
  const [textMessage, setTextMessage] = useState("");

  const handleEdit = () => {
    setDialogTitle("Edit Account");
    setShowDialogProgress(true);
    setShowDialog(true);
    const details = {
      fullName: fullName.current.value,
      email: email.current.value,
      password: password.current.value,
    };
    apiService.UsersService.editUser(details)
      .then((response) => {
        if (response.status === 200) {
          setShowDialogProgress(false);
          setTextMessage(response.data.message);
        }
      })
      .catch((error) => {
        setDialogTitle("Editing Failed");
        setTextMessage(error.response.data.message);
        setShowDialogProgress(false);
      });
  };

  const closeAlertHandler = () => {
    setShowDialog(false);
  };

  const closeAlertHandlerRedirect = () => {
    setShowDialog(false);
    navigate("/");
  };

  return (
    <div className={style.settings_container}>
      <Typography variant="h2" component="div" className={style.main_headline}>
        User Settings
      </Typography>
      <Typography variant="h4" component="div">
        Edit Account's Details
      </Typography>
      <div className={style.section_container}>
        <div className={style.fields_container}>
          <TextField
            id="outlined-basic"
            label="Edit Name"
            variant="outlined"
            inputRef={fullName}
          />
          <TextField
            id="outlined-basic"
            label="Edit Email"
            variant="outlined"
            inputRef={email}
          />
          <TextField
            id="outlined-basic"
            label="Edit Password"
            variant="outlined"
            inputRef={password}
          />
        </div>
        <Button
          variant="contained"
          className={style.edit_button}
          onClick={handleEdit}
        >
          Save Changes
        </Button>
      </div>
      <Divider flexItem={true} role="presentation" />
      <Typography
        variant="h4"
        component="div"
        className={style.section_headline}
      >
        Delete Account
      </Typography>
      <div className={style.section_container}>
        <Typography variant="h6" component="div">
          Remember, this action cannot be undone!
        </Typography>
        <DeleteButton />
      </div>
      <AlertDialog
        showDialog={showDialog}
        showDialogProgress={showDialogProgress}
        title={dialogTitle}
        closeHandler={closeAlertHandler}
        closeHandlerRedirect={closeAlertHandlerRedirect}
        redirectMessage="Go to home page"
        showRedirectButton={true}
        description={textMessage}
      />
    </div>
  );
};

export default UserSettings;
