import { Link } from "react-router-dom";
import logo from "../../../Resources/naruto.jpeg";
import style from "./navbar.module.css";
import SearchBar from "./searchBar";
import UserPanel from "./userPanel";

const Navbar = ({ children: themeToggleButton }) => {

  return (
    <div className={style.navbar_container}>
      <div className={style.navbar}>
        <Link className={style.navbar_logo_container} to={"/"}>
          <img className={style.navbar_logo} src={logo} alt="logo" />
          <div className={style.navbar_headline}>My Anime Tracker</div>
        </Link>

        <SearchBar />

        <div className={style.theme_toggle}>{themeToggleButton}</div>
        <div className={style.navbar_user}>
          <UserPanel />
        </div>
      </div>
    </div>
  );
};

export default Navbar;
