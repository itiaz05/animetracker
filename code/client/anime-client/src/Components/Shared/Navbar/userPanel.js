import CollectionsBookmarkIcon from "@mui/icons-material/CollectionsBookmark";
import Logout from "@mui/icons-material/Logout";
import Settings from "@mui/icons-material/Settings";
import { Avatar, Button, IconButton, ListItemIcon, Menu, MenuItem, Tooltip } from "@mui/material";
import React, { useCallback, useMemo, useState } from "react";
import { useNavigate } from "react-router-dom";
import apiService from "../../../Services/API/api";
import useLogout from "../../../Services/Auth/Hooks/useLogout";
import { useAuth } from "../../../Services/Auth/authProvider";

const UserPanel = React.memo(() => {
  const { auth } = useAuth();
  const handleLogout = useLogout();
  const [anchorEl, setAnchorEl] = useState(null);
  const navigate = useNavigate();
  const open = Boolean(anchorEl);

  const handleClick = useCallback((event) => {
    setAnchorEl(event.currentTarget);
  }, []);

  const handleClose = useCallback(
    async (pageToNavigate) => {
      setAnchorEl(null);
      if (pageToNavigate?.path) {
        const desiredPath = pageToNavigate.path.toString();
        if (desiredPath === "/myList") {
          const response = await apiService.SeriesService.getMyList();
          navigate(desiredPath, {
            state: {
              list: response.list.map((anime) => anime.animeId),
              length: response.count,
              genre: "My List"
            }
          });
          return;
        }
        if (desiredPath === "/") {
          await handleLogout();
        }
        navigate(desiredPath);
      }
    },
    [navigate, handleLogout]
  );

  const menuItems = useMemo(
    () => [
      { label: "My List", icon: <CollectionsBookmarkIcon fontSize="small" />, path: "/myList" },
      { label: "Settings", icon: <Settings fontSize="small" />, path: "/settings" },
      { label: "Logout", icon: <Logout fontSize="small" />, path: "/" }
    ],
    []
  );

  const renderMenuItem = useCallback(
    (item) => (
      <MenuItem key={item.label} onClick={() => handleClose({ path: item.path })}>
        <ListItemIcon>{item.icon}</ListItemIcon>
        {item.label}
      </MenuItem>
    ),
    [handleClose]
  );

  const renderUserMenu = () => (
    <div className="logout_container">
      <Tooltip title="Account settings">
        <IconButton
          onClick={handleClick}
          size="small"
          sx={{ ml: 2 }}
          aria-controls={open ? "account-menu" : undefined}
          aria-haspopup="true"
          aria-expanded={open ? "true" : undefined}
        >
          <Avatar sx={{ width: 32, height: 32 }}>{auth?.user?.charAt(0).toUpperCase()}</Avatar>
        </IconButton>
      </Tooltip>
      <Menu anchorEl={anchorEl} id="account-menu" open={open} onClose={() => handleClose()} onClick={() => handleClose()}>
        {menuItems.map(renderMenuItem)}
      </Menu>
    </div>
  );

  const renderLoginButtons = () => (
    <div className="login_container">
      <Button variant="contained" onClick={() => navigate("/login")}>
        Login
      </Button>
      <Button variant="contained" onClick={() => navigate("/signUp")}>
        Sign Up
      </Button>
    </div>
  );

  return <div className="user_panel">{auth?.user ? renderUserMenu() : renderLoginButtons()}</div>;
});

export default UserPanel;
