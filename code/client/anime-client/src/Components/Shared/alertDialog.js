import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

const AlertDialog = ({
  showDialog,
  showProgress,
  closeHandler,
  closeHandlerRedirect = null,
  redirectMessage = "Redirect",
  showRedirectButton = false,
  title,
  description,
}) => {
  return (
    <>
      <Dialog
        open={showDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{`${title}`}</DialogTitle>
        <DialogContent>
          {showProgress ? (
            <CircularProgress />
          ) : (
            <DialogContentText id="alert-dialog-description">
              {`${description}`}
            </DialogContentText>
          )}
        </DialogContent>
        <DialogActions>
          {showRedirectButton && (
            <Button onClick={closeHandlerRedirect} disabled={showProgress}>
              {redirectMessage}
            </Button>
          )}
          <Button autoFocus onClick={closeHandler} disabled={showProgress}>
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default AlertDialog;
