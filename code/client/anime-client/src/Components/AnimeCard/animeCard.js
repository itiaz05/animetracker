import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import style from "./animeCard.module.css";
import useGenres from "../../Services/Hooks/useGenres";

const AnimeCard = ({ details }) => {
  const [hover, setHover] = useState(false);
  const navigate = useNavigate();
  const [info, setInfo] = useState({});
  const { destructureGenres } = useGenres();

  useEffect(() => {
    if (details) {
      const genresToSet = destructureGenres(details.genres);
      setInfo({ ...details, genres: genresToSet });
    }
  }, [details, destructureGenres]);

  return (
    <div
      className={style.container}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      onMouseDown={() =>
        navigate(`/animePage/${info.title}`, { state: details })
      }
    >
      {(hover && (
        <div className={style.backCard}>
          <p>Year: {info.year}</p>
          <p>
            Episodes:{" "}
            {info.numberOfEpisodes === 0 ? "Unknown" : info.numberOfEpisodes}
          </p>
          <p>Rating: {info?.rating}</p>
          <p>Status: {info?.status}</p>
          <p className={style.genres}>Genres: {info?.genres?.join(", ")}</p>
        </div>
      )) || (
        <div className={style.details}>
          <p className={style.title}>{info.title}</p>
          <img className={style.img} src={info.image} alt={info.title} />
        </div>
      )}
    </div>
  );
};

export default AnimeCard;
